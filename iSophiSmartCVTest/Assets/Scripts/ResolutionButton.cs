using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ResolutionButton : Button, IPointerClickHandler, IOnWebCamStart, IOnWebCamStop
{
    
    [SerializeField]
    private Text titleText;

    private Resolution resolution;
    
    public void Init(Resolution aResolution)
    {
        resolution = aResolution;

        titleText.text = $"{resolution.width}x{resolution.height}";
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        ExecuteEvents.ExecuteHierarchy<IOnResolutionSelected>(gameObject, null,
            (handler, data) => handler.OnResolutionSelected(resolution));
    }

    public void OnWebCamStart(WebCamDevice webCamDevice, Resolution aResolution, WebCamTexture webCamTexture)
    {
        SetSelected(resolution.Equals(aResolution));
    }

    public void OnWebCamStop()
    {
        SetSelected(false);
    }
}
