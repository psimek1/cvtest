using UnityEngine;
using UnityEngine.EventSystems;

public class ModeButton : Button, IPointerClickHandler, IOnModeSelected
{

    [SerializeField] 
    private RecognitionMode mode;

    public void OnPointerClick(PointerEventData eventData)
    {
        ExecuteEvents.ExecuteHierarchy<IOnModeClick>(gameObject, null, (handler, data) => handler.OnModeClick(mode));
    }

    public void OnModeSelected(RecognitionMode aMode)
    {
        SetSelected(mode == aMode);
    }
    
}
