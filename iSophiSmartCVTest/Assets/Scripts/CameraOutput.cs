using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CameraOutput : MonoBehaviour, IOnWebCamStart, IOnWebCamStop, IPointerClickHandler
{

    [SerializeField] 
    private RawImage rawImage;
    
    [SerializeField]
    private AspectRatioFitter aspectRatioFitter;

    [SerializeField] 
    private Text commentText;
    
    private WebCamTexture webCamTexture;

    private bool cameraRunning;

    private bool cameraOutputVisible;
    
    private void Awake()
    {
        rawImage.gameObject.SetActive(false);
    }

    public void OnWebCamStart(WebCamDevice aWebCamDevice, Resolution resolution, WebCamTexture aWebCamTexture)
    {
        webCamTexture = aWebCamTexture;
        aspectRatioFitter.aspectRatio = (float)webCamTexture.width / webCamTexture.height;
        cameraRunning = true;
        cameraOutputVisible = true;
        UpdateVisibility();
    }

    private void Update()
    {
        if (!cameraRunning || !cameraOutputVisible)
        {
            return;
        }
        
        #if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
            rawImage.texture = webCamTexture;
        #endif
    }

    public void OnWebCamStop()
    {
        cameraOutputVisible = false;
        UpdateVisibility();
        webCamTexture = null;
        cameraRunning = false;
        commentText.text = "";
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!cameraRunning) return;
        cameraOutputVisible = !cameraOutputVisible;
        UpdateVisibility();
    }

    private void UpdateVisibility()
    {
        rawImage.gameObject.SetActive(cameraOutputVisible);
        rawImage.texture = cameraOutputVisible ? webCamTexture : null;
        commentText.text = "Tap to " + (cameraOutputVisible ? "hide" : "show") + " camera output";
    }

}
