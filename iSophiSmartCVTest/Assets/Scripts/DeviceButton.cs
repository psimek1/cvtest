using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DeviceButton : Button, IPointerClickHandler, IOnWebCamDeviceSelected
{

    private WebCamDevice webCamDevice;

    [SerializeField]
    private Text titleText;

    [SerializeField]
    private Text infoText;

    public void Init(WebCamDevice aWebCamDevice)
    {
        webCamDevice = aWebCamDevice;

        var availableResolutions = webCamDevice.availableResolutions == null
            ? ""
            : string.Join(",", webCamDevice.availableResolutions);
        
        Log.Add($"WebCamDevice added:");
        Log.Add($"name={webCamDevice.name}");
        Log.Add($"kind={webCamDevice.kind}");
        Log.Add($"isFrontFacing={webCamDevice.isFrontFacing}");
        Log.Add($"isAutoFocusPointSupported={webCamDevice.isAutoFocusPointSupported}");
        Log.Add($"depthCameraName={webCamDevice.depthCameraName}");
        Log.Add($"availableResolutions={availableResolutions}");
        Log.AddLine();
        
        titleText.text = webCamDevice.name;

        var info = webCamDevice.isFrontFacing ? "Front" : "Back";
        info += "\n";

        infoText.text = info;
    }

    public void Select()
    {
        ExecuteEvents.ExecuteHierarchy<IOnWebCamDeviceClick>(gameObject, null,
            (handler, data) => handler.OnWebCamDeviceClick(webCamDevice));
    }
    
    public void OnPointerClick(PointerEventData eventData)
    {
        Select();
    }

    public void OnWebCamDeviceSelected(WebCamDevice aWebCamDevice)
    {
        SetSelected(webCamDevice.Equals(aWebCamDevice));
    }
}
