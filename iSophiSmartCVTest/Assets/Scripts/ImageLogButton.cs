using UnityEngine;
using UnityEngine.EventSystems;

public class ImageLogButton : MonoBehaviour, IPointerClickHandler
{

    public void OnPointerClick(PointerEventData eventData)
    {
        ExecuteEvents.ExecuteHierarchy<IOnImageLogClick>(gameObject, null, (handler, data) => handler.OnImageModeClick());
    }

}
