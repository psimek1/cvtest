using UnityEngine;
using UnityEngine.UI;

public abstract class Button : MonoBehaviour
{
    
    private Image image;
    
    private float deselectedAlpha = 0.3f;

    private bool isSelected;

    private void Awake()
    {
        image = GetComponentInChildren<Image>();
        SetSelected(false);
    }
    
    protected void SetSelected(bool value)
    {
        isSelected = false;
        var c = image.color;
        image.color = new Color(c.r, c.g, c.b, value ? 1 : deselectedAlpha);
    }
    
}
