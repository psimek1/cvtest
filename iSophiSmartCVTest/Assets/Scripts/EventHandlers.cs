using UnityEngine;
using UnityEngine.EventSystems;

public interface IOnWebCamDevicesAdded: IEventSystemHandler
{
    void OnWebCamDevicesAdded();
}

public interface IOnWebCamDeviceClick: IEventSystemHandler
{
    void OnWebCamDeviceClick(WebCamDevice webCamDevice);
}

public interface IOnWebCamDeviceSelected
{
    void OnWebCamDeviceSelected(WebCamDevice webCamDevice);
}

public interface IOnResolutionSelected: IEventSystemHandler
{
    void OnResolutionSelected(Resolution resolution);
}

public interface IOnWebCamStart
{
    void OnWebCamStart(WebCamDevice webCamDevice, Resolution resolution, WebCamTexture webCamTexture);
}

public interface IOnWebCamStop
{
    void OnWebCamStop();
}

public interface IOnModeClick: IEventSystemHandler
{
    void OnModeClick(RecognitionMode mode);
}

public interface IOnModeSelected
{
    void OnModeSelected(RecognitionMode mode);
}

public interface IOnImageLogClick: IEventSystemHandler
{
    void OnImageModeClick();
}

