using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Resolutions : MonoBehaviour, IOnWebCamDeviceSelected
{

    [SerializeField]
    private GameObject resolutionPrefab;

    [SerializeField] 
    private Transform hGroup;

    [SerializeField]
    private ScrollRect scrollRect;
    
    private readonly Resolution[] defaultResolutions = 
        {
            new Resolution{width = 1440, height = 1080}, 
            new Resolution{width = 720, height = 540}, 
            new Resolution{width = 1, height = 1}, 
        };
    
    public void OnWebCamDeviceSelected(WebCamDevice webCamDevice)
    {
        var childCount = hGroup.childCount;
        for (var i = 0; i < childCount; i++)
        {
            Destroy(hGroup.GetChild(i).gameObject);
        }

        if (webCamDevice.name == null)
        {
            return;
        }
        
        var resolutions = webCamDevice.availableResolutions ?? defaultResolutions;

        var savedResolution = ResolutionExt.FromInt(PlayerPrefs.GetInt(Main.Resolution + webCamDevice.name));
        
        foreach (var resolution in resolutions)
        {
            var go = Instantiate(resolutionPrefab, hGroup);
            go.GetComponent<ResolutionButton>().Init(resolution);
            if (resolution.Equals(savedResolution))
            {
                go.transform.SetAsFirstSibling();
                ExecuteEvents.ExecuteHierarchy<IOnResolutionSelected>(gameObject, null,
                    (handler, data) => handler.OnResolutionSelected(resolution));
            }
        }
        
        scrollRect.normalizedPosition = Vector2.zero;
    }
    
}
