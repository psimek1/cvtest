using System.Collections;
using System.Collections.Generic;
using System.Linq;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.UnityUtils;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ImageLog : MonoBehaviour, IPointerClickHandler
{

    private const int MaxBufferSize = 50 * 1024 * 1024; // pixel count, not bytes
    
    [SerializeField]
    private RawImage rawImage;
    
    [SerializeField]
    private AspectRatioFitter aspectRatioFitter;

    [SerializeField] 
    private GameObject prevButton;

    [SerializeField] 
    private GameObject nextButton;
    
    [SerializeField] 
    private GameObject clearButton;
    
    [SerializeField] 
    private Text counterText;

    [SerializeField] 
    private GameObject statusText;

    private List<Mat> mats;

    private Texture2D texture;
    
    private int matIndex;

    private bool refreshInProgress;
    
    private void Awake()
    {
        gameObject.SetActive(false);
        mats = new List<Mat>();
        matIndex = -1;
        statusText.SetActive(false);
    }

    public void AddImage(Mat mat)
    {
        var size = mat.width() * mat.height();

        while (mats.Sum(m => m.width()*m.height()) + size > MaxBufferSize)
        {
            mats.RemoveAt(0);
        }
        
        mats.Add(mat.clone());
        
        matIndex = mats.Count - 1;
        
        Refresh();
    }
    
    public void ClearImages()
    {
        mats.Clear();
        matIndex = -1;
        Refresh();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.hovered.Contains(prevButton))
        {
            if (matIndex <= 0) return;
            matIndex--;
            Refresh();
        }
        else if (eventData.hovered.Contains(nextButton))
        {
            if (matIndex >= mats.Count-1) return;
            matIndex++;
            Refresh();
        }
        else if (eventData.hovered.Contains(clearButton))
        {
            ClearImages();
        }
    }
    
    private void OnEnable()
    {
        matIndex = mats.Count - 1;
        Refresh();
    }
    
    private void OnDisable()
    {
        matIndex = -1;
        Refresh();
    }

    private void Refresh()
    {
        if (!gameObject.activeInHierarchy) return;

        StartCoroutine(RefreshCoroutine());
    }

    private IEnumerator RefreshCoroutine()
    {

        while (refreshInProgress)
        {
            yield return null;
        }

        refreshInProgress = true;
        
        counterText.text = $"{matIndex + 1} / {mats?.Count ?? 0}";
        prevButton.GetComponent<CanvasGroup>().alpha = matIndex > 0 ? 1 : 0.3f;
        nextButton.GetComponent<CanvasGroup>().alpha = (mats != null && matIndex < mats.Count-1) ? 1 : 0.3f;
        
        yield return null;
        
        rawImage.texture = null;
        texture = null;
        Destroy(rawImage.texture);

        yield return null;
        
        Resources.UnloadUnusedAssets();

        yield return null;

        if (matIndex < 0) yield break;
        
        statusText.SetActive(true);
        
        var mat = mats[matIndex];
        if (texture == null || texture.width != mat.width() || texture.height != mat.height())
        {
            texture = new Texture2D(mat.width(), mat.height());
        }
        
        yield return null;

        Utils.matToTexture2D(mat, texture);
        
        yield return null;
        
        texture.Apply(true, true);
        
        yield return null;
        
        rawImage.texture = texture;
        aspectRatioFitter.aspectRatio = (float) texture.width / texture.height;
        
        statusText.SetActive(false);

        refreshInProgress = false;
    }
    
}
