using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class Devices : MonoBehaviour
{

    [SerializeField]
    private GameObject deviceButtonPrefab;

    private void Start()
    {
        StartCoroutine(StartCoroutine());
    }

    private IEnumerator StartCoroutine()
    {
        Log.Add("Waiting for camera devices.");
        
        WebCamDevice[] devices;
        while ((devices = WebCamTexture.devices).Length == 0)
        {
            yield return null;
        }
        
        foreach (var webCamDevice in devices)
        {
            Instantiate(deviceButtonPrefab, transform).GetComponent<DeviceButton>().Init(webCamDevice);
        }

        ExecuteEvents.ExecuteHierarchy<IOnWebCamDevicesAdded>(gameObject, null,
            (handler, data) => handler.OnWebCamDevicesAdded());
    }
    
}
