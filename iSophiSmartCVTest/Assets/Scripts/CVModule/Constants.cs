﻿using System;
using System.Collections.Generic;
using OpenCVForUnity;
using OpenCVForUnity.CoreModule;

namespace CVModule
{
    /// <summary>
    /// PART OF PUBLIC API
    /// Enum that includes all parts of iSophiSmart
    /// </summary>
    public enum CvTile
    {
        Unknown = -1,
        
        SqFullYellow = 0,
        SqFullBlue = 1,
        SqFullGreen = 2,
        SqFullRed = 3,
        
        SqStripeYellow = 4,
        SqStripeBlue = 5, 
        SqStripeGreen = 6,
        
        SqDiagYellow = 7,
        SqDiagGreen = 8, 
        SqDiagRed = 9,
        
        SqHomeYellow = 10,
        SqHomeBlue = 11,
        SqHomeGreen = 12, 
        SqHomeRed = 13, 
        SqHomeBlack = 14,
        
        DotYellow = 15,
        DotBlue = 16,
        DotGreen = 17,
        DotRed = 18,
        
        TrioSSRed = 19,
        TrioCSRed = 20,
        TrioSSYellow = 21, 
        TrioCSYellow = 22,
        TrioSSGreen = 23, 
        TrioCSGreen = 24,
        
        SqMove = 25,
        SqCarry = 26,
        SqRotate = 27,
        SqClear = 28,
        SqRepeat2 = 29,
        SqRepeat3 = 30,
        
        SqCoord1 = 31,
        SqCoord2 = 32,
        SqCoord3 = 33,
        SqCoord4 = 34,
        
        SqTriangle = 35,
        BarBlue2 = 36,
        BarBlue3 = 37,
        BarBlue4 = 38,
        BarBlue5 = 39,
        BarBlue6 = 40,
        BarBlue7 = 41,
        BarBlue8 = 42,
    }
    
    /// <summary>
    /// Extension for CvTile enum.
    /// </summary>
    static class CvTileMethods
    {
        // return type and color of tile
        internal static (TileType, Color) TypeAndColor(this CvTile tile)
        {
            TileType type;
            Color c;
            switch (tile)
            {
                case CvTile.SqFullYellow:
                    return (TileType.Solid, Color.Yellow);
                case CvTile.SqFullBlue:
                    return (TileType.Solid, Color.Blue);
                case CvTile.SqFullGreen:
                    return (TileType.Solid, Color.Green);
                case CvTile.SqFullRed:
                    return (TileType.Solid, Color.Red);
                case CvTile.SqStripeYellow:
                    return (TileType.Stripe, Color.Yellow);
                case CvTile.SqStripeBlue:
                    return (TileType.Stripe, Color.Blue);
                case CvTile.SqStripeGreen:
                    return (TileType.Stripe, Color.Green);
                case CvTile.SqDiagYellow:
                    return (TileType.Diag, Color.Yellow);
                case CvTile.SqDiagGreen:
                    return (TileType.Diag, Color.Green);
                case CvTile.SqDiagRed:
                    return (TileType.Diag, Color.Red);
                case CvTile.SqHomeYellow:
                    return (TileType.House, Color.Yellow);
                case CvTile.SqHomeBlue:
                    return (TileType.House, Color.Blue);
                case CvTile.SqHomeGreen:
                    return (TileType.House, Color.Green);
                case CvTile.SqHomeRed:
                    return (TileType.House, Color.Red);
                case CvTile.SqHomeBlack:
                    return (TileType.House, Color.Black);
                case CvTile.DotYellow:
                    return (TileType.Dot, Color.Yellow);
                case CvTile.DotBlue:
                    return (TileType.Dot, Color.Blue);
                case CvTile.DotGreen:
                    return (TileType.Dot, Color.Green);
                case CvTile.DotRed:
                    return (TileType.Dot, Color.Red);
                case CvTile.TrioSSRed:
                    return (TileType.TriangleSS, Color.Red);
                case CvTile.TrioCSRed:
                    return (TileType.TriangleCS, Color.Red);
                case CvTile.TrioSSYellow:
                    return (TileType.TriangleSS, Color.Yellow);
                case CvTile.TrioCSYellow:
                    return (TileType.TriangleCS, Color.Yellow);
                case CvTile.TrioSSGreen:
                    return (TileType.TriangleSS, Color.Green);
                case CvTile.TrioCSGreen:
                    return (TileType.TriangleCS, Color.Green);
                case CvTile.SqMove:
                    return (TileType.Move, Color.Unknown);
                case CvTile.SqCarry:
                    return (TileType.Carry, Color.Unknown);
                case CvTile.SqRotate:
                    return (TileType.Rotate, Color.Unknown);
                case CvTile.SqClear:
                    return (TileType.Clear, Color.Unknown);
                case CvTile.SqRepeat2:
                    return (TileType.Repeat2, Color.Unknown);
                case CvTile.SqRepeat3:
                    return (TileType.Repeat3, Color.Unknown);
                case CvTile.SqCoord1:
                    return (TileType.Coord1, Color.Unknown);
                case CvTile.SqCoord2:
                    return (TileType.Coord2, Color.Unknown);
                case CvTile.SqCoord3:
                    return (TileType.Coord3, Color.Unknown);
                case CvTile.SqCoord4:
                    return (TileType.Coord4, Color.Unknown);
                case CvTile.SqTriangle:
                    return (TileType.Triangle, Color.Unknown);
                case CvTile.BarBlue2:
                    return (TileType.BarBlue2, Color.Unknown);
                case CvTile.BarBlue3:
                    return (TileType.BarBlue3, Color.Unknown);
                case CvTile.BarBlue4:
                    return (TileType.BarBlue4, Color.Unknown);
                case CvTile.BarBlue5:
                    return (TileType.BarBlue5, Color.Unknown);
                case CvTile.BarBlue6:
                    return (TileType.BarBlue6, Color.Unknown);
                case CvTile.BarBlue7:
                    return (TileType.BarBlue7, Color.Unknown);
                case CvTile.BarBlue8:
                    return (TileType.BarBlue8, Color.Unknown);
                default:
                    return (TileType.Unknown, Color.Unknown);
                    // throw new ArgumentOutOfRangeException(nameof(tile), tile, null);
            }
        }

        internal static CvTile GetCvTile(this string name)
        {
            switch (name)
            {
                case "SqFullYellow":
                    return CvTile.SqFullYellow;
                case "SqFullBlue":
                    return CvTile.SqFullBlue;
                case "SqFullGreen":
                    return CvTile.SqFullGreen;
                case "SqFullRed":
                    return CvTile.SqFullRed;
                case "SqStripeYellow":
                    return CvTile.SqStripeYellow;
                case "SqStripeBlue":
                    return CvTile.SqStripeBlue;
                case "SqStripeGreen":
                    return CvTile.SqStripeGreen;
                case "SqDiagYellow":
                    return CvTile.SqDiagYellow;
                case "SqDiagGreen":
                    return CvTile.SqDiagGreen;
                case "SqDiagRed":
                    return CvTile.SqDiagRed;
                case "SqHomeYellow":
                    return CvTile.SqHomeYellow;
                case "SqHomeBlue":
                    return CvTile.SqHomeBlue;
                case "SqHomeGreen":
                    return CvTile.SqHomeGreen;
                case "SqHomeRed":
                    return CvTile.SqHomeRed;
                case "SqHomeBlack":
                    return CvTile.SqHomeBlack;
                case "DotYellow":
                    return CvTile.DotYellow;
                case "DotBlue":
                    return CvTile.DotBlue;
                case "DotGreen":
                    return CvTile.DotGreen;
                case "DotRed":
                    return CvTile.DotRed;
                case "TrioSSRed":
                    return CvTile.TrioSSRed;
                case "TrioCSRed":
                    return CvTile.TrioCSRed;
                case "TrioSSYellow":
                    return CvTile.TrioSSYellow;
                case "TrioCSYellow":
                    return CvTile.TrioCSYellow;
                case "TrioSSGreen":
                    return CvTile.TrioSSGreen;
                case "TrioCSGreen":
                    return CvTile.TrioCSGreen;
                case "SqMove":
                    return CvTile.SqMove;
                case "SqCarry":
                    return CvTile.SqCarry;
                case "SqRotate":
                    return CvTile.SqRotate;
                case "SqClear":
                    return CvTile.SqClear;
                case "SqRepeat2":
                    return CvTile.SqRepeat2;
                case "SqRepeat3":
                    return CvTile.SqRepeat3;
                case "SqCoord1":
                    return CvTile.SqCoord1;
                case "SqCoord2":
                    return CvTile.SqCoord2;
                case "SqCoord3":
                    return CvTile.SqCoord3;
                case "SqCoord4":
                    return CvTile.SqCoord4;
                case "SqTriangle":
                    return CvTile.SqTriangle;
                case "BarBlue2":
                    return CvTile.BarBlue2;
                case "BarBlue3":
                    return CvTile.BarBlue3;
                case "BarBlue4":
                    return CvTile.BarBlue4;
                case "BarBlue5":
                    return CvTile.BarBlue5;
                case "BarBlue6":
                    return CvTile.BarBlue6;
                case "BarBlue7":
                    return CvTile.BarBlue7;
                case "BarBlue8":
                    return CvTile.BarBlue8;
                default:
                    return CvTile.Unknown; 
                    // throw new ArgumentOutOfRangeException(nameof(name), name, null);
            }
        }
    }
    
    /// <summary>
    /// PART OF PUBLIC API
    /// Enum to keep track of area types.
    /// </summary>
    public enum AreaType
    {
        Grid4X4 = 0,
        Grid8X2 = 1,
        SolidBlue = 2,
        StripsRed = 3,
        None = -1
    }
    
    /// <summary>
    /// Extension for AreaType enum.
    /// </summary>
    static class AreaTypeMethods
    {
        internal static string GetString(this AreaType at)
        {
            switch (at)
            {
                case AreaType.Grid4X4:
                    return "Grid4x4";
                case AreaType.Grid8X2:
                    return "Grid8x2";
                default:
                    return "Unknown";
            }
        }

        internal static bool IsGrid(this AreaType at)
        {
            return at == AreaType.Grid4X4 || at == AreaType.Grid8X2;
        }
        
        internal static AreaType GetAreaType(this string areaName)
        {
            switch (areaName)
            {
                case "Grid4x4": return AreaType.Grid4X4;
                case "Grid8x2": return AreaType.Grid4X4;
                default: return AreaType.None;
            }
        }
    }

    /// <summary>
    /// Enum that keeps track of colors of tiles.
    /// </summary>
    internal enum Color
    {
        Unknown = -1,
        
        Red = 0, 
        Green = 1,
        Blue = 2,
        Yellow = 3,
        Black = 4
    }

    /// <summary>
    /// Extension of Color enum.
    /// </summary>
    static class ColorMethods
    {
        internal static List<Scalar> BoundsValue(this Color c)
        {
            switch (c)
            {
                case Color.Red: return Constants.RED;
                case Color.Blue: return Constants.BLUE;
                case Color.Green: return Constants.GREEN;
                case Color.Yellow: return Constants.YELLOW;
                default: return new List<Scalar>();
            }
        }
        internal static Scalar Value(this Color c)
        {
            switch (c)
            {
                case Color.Black: return new Scalar(0, 0, 0);
                case Color.Blue: return new Scalar(255, 0, 0);
                case Color.Green: return new Scalar(0, 255, 0);
                case Color.Red: return new Scalar(0, 0, 255);
                case Color.Yellow: return new Scalar(255, 255, 0);
                default: return new Scalar(0, 0, 0);
            }
        }
        internal static string GetString(this Color c)
        {
            switch (c)
            {
                case Color.Black: return "Black";
                case Color.Blue: return "Blue";
                case Color.Green: return "Green";
                case Color.Red: return "Red";
                case Color.Yellow: return "Yellow";
                default: return "";
            }
        }
    }

    /// <summary>
    /// TileType to keep track of tile types.
    /// </summary>
    internal enum TileType
    {
        Unknown = -1,
        
        Carry = 0,
        Clear = 1,
        Coord1 = 2,
        Coord2 = 3,
        Coord3 = 4,
        Coord4 = 5,
        House = 6,
        Move = 7,
        Repeat2 = 8,
        Repeat3 = 9,
        Rotate = 10,
        Solid = 12,
        Stripe = 13,
        TriangleSS = 14,
        TriangleCS = 15,
        Diag = 16,
        Dot = 17,
        Triangle = 18,
        BarBlue2 = 19,
        BarBlue3 = 20,
        BarBlue4 = 21,
        BarBlue5 = 22,
        BarBlue6 = 23,
        BarBlue7 = 24,
        BarBlue8 = 25,
    }

    /// <summary>
    /// Extension for TileType enum.
    /// </summary>
    static class TileTypeMethods
    {
        public static string GetString(this TileType tt)
        {
            switch (tt)
            {
                case TileType.Carry:
                    return "SqCarry";
                case TileType.Clear:
                    return "SqClear";
                case TileType.Coord1:
                    return "SqCoord1";
                case TileType.Coord2:
                    return "SqCoord2";
                case TileType.Coord3:
                    return "SqCoord3";
                case TileType.Coord4:
                    return "SqCoord4";
                case TileType.House:
                    return "SqHome";
                case TileType.Move:
                    return "SqMove";
                case TileType.Repeat2:
                    return "SqRepeat2";
                case TileType.Repeat3:
                    return "SqRepeat3";
                case TileType.Rotate:
                    return "SqRotate";
                case TileType.Solid:
                    return "SqFull";
                case TileType.Stripe:
                    return "SqStripe";
                case TileType.TriangleSS:
                    return "TrioSS";
                case TileType.TriangleCS:
                    return "TrioCS";
                case TileType.Diag:
                    return "SqDiag";
                case TileType.Dot:
                    return "Dot";
                case TileType.Triangle:
                    return "SqTriangle";
                case TileType.BarBlue2:
                    return "BarBlue2";
                case TileType.BarBlue3:
                    return "BarBlue3";
                case TileType.BarBlue4:
                    return "BarBlue4";
                case TileType.BarBlue5:
                    return "BarBlue5";
                case TileType.BarBlue6:
                    return "BarBlue6";
                case TileType.BarBlue7:
                    return "BarBlue7";
                case TileType.BarBlue8:
                    return "BarBlue8";
                default:
                    throw new ArgumentOutOfRangeException(nameof(tt), tt, null);
            }
        }
    }

    internal enum Rotation
    {
        deg0 = 0,
        deg90 = 90,
        deg180 = 180,
        deg270 = 270,
        deg360 = 360
    }

    static class RotationExtensions
    {
        public static int Value(this Rotation r)
        {
            switch (r)
            {
                case Rotation.deg0:
                    return 0;
                case Rotation.deg90:
                    return 90;
                case Rotation.deg180:
                    return 180;
                case Rotation.deg270:
                    return 270;
                case Rotation.deg360:
                    return 360;
                default:
                    throw new ArgumentOutOfRangeException(nameof(r), r, null);
            }
        } 
    }
    
    
    /// <summary>
    /// All constants.
    /// </summary>
    internal static class Constants
    {
        // Color values
        internal static List<Scalar> RED = new List<Scalar> {
            new Scalar(Utilities.ToIntensity(0),   50,  50),
            new Scalar(Utilities.ToIntensity(30),  255, 255),
            new Scalar(Utilities.ToIntensity(330), 50,  50),
            new Scalar(Utilities.ToIntensity(360), 255, 255)
        };
        internal static List<Scalar> BLUE = new List<Scalar> {
            new Scalar(Utilities.ToIntensity(195), 20,  30),
            new Scalar(Utilities.ToIntensity(245), 200, 225)
        };
        internal static List<Scalar> GREEN = new List<Scalar> {
            new Scalar(Utilities.ToIntensity(90), 20,  0),
            new Scalar(Utilities.ToIntensity(150), 255, 255)
        };
        internal static List<Scalar> YELLOW = new List<Scalar> {
            new Scalar(Utilities.ToIntensity(30), 70,  70),
            new Scalar(Utilities.ToIntensity(70), 255, 255)
        };

        // Calibration points
        internal static double IMG_COEF = 1.2;

        private static List<Point> realPoints = new List<Point> {
            new Point(165  * IMG_COEF,  3090 * IMG_COEF),
            new Point(2805 * IMG_COEF,  3090 * IMG_COEF),
            new Point(825  * IMG_COEF,  490  * IMG_COEF),
            new Point(2145 * IMG_COEF,  490  * IMG_COEF)
        };

        internal static int minCoef = 12000;
        internal static int maxCoef = 200;
        
        internal static MatOfPoint2f realPointsMat = new MatOfPoint2f(realPoints.ToArray());

        // Some magical constants for calibration for minimal product
        internal static List<int> REF_MARK_COORD = new List<int> { 1779, 2570 };
        internal static int TILESIZE = 385;
        internal static int FittingGridToler = 70;
        internal static double RefWarpedImgWidth = 3100 * Constants.IMG_COEF;
        internal static double RefWarpedImgHeight = 3600 * Constants.IMG_COEF;
        internal static int MarkToleration = 500;
        internal static int GridPositionTolerance = 60;
        internal static int PatternCrop = 60;
        internal static int PatternAngleTolerance = 6;

        internal static List<Rotation> Rotations = new List<Rotation>
        {
            Rotation.deg0,
            Rotation.deg90,
            Rotation.deg180,
            Rotation.deg270,
            Rotation.deg360
        };
    }
}
