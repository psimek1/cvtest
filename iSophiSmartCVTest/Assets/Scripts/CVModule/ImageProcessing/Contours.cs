﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.ImgprocModule;
using OpenCVForUnity.Calib3dModule;
using OpenCVForUnity.ImgcodecsModule;
using UnityEditor;

namespace CVModule
{
    internal class Contours
    {
        internal static List<MatOfPoint> MyFindContours(Mat binImage, int minArea=0, int maxArea=10000, bool external=true, int thickness=10)
        {
            var contours = new List<MatOfPoint>();
            Mat hierarchy = new Mat();
            int mode = external ? Imgproc.RETR_EXTERNAL : Imgproc.RETR_LIST;
            var foundContours = new List<MatOfPoint>();

            Imgproc.findContours(binImage, foundContours, hierarchy, mode, Imgproc.CHAIN_APPROX_SIMPLE);
            
            foreach (var cnt in foundContours)
            {
                double area = Imgproc.contourArea(cnt);
                if (minArea < area && area < maxArea)
                {
                    contours.Add(cnt);
                }
            }
            return contours;
        }

        internal static void Draw(Mat img, List<MatOfPoint> contours, int contourIdx = -1, Color c = Color.Red,
            int thickness = 3)
        {
            var drawImg = img.clone();
            Imgproc.drawContours(drawImg, contours, contourIdx, c.Value(), thickness: thickness);
            Log.AddImage(drawImg);
        }

        internal static MatOfPoint ApproxContour(MatOfPoint cnt, int minPoints=4, int maxPoints=8)
        {
            double precision = 10000;
            var correctFormatCnt = new MatOfPoint2f(cnt.toArray());

            for (int i = 1; i < (int)precision; i++)
            {
                var approxCurve = new MatOfPoint2f();
                var eps = i / precision * Imgproc.arcLength(correctFormatCnt, true);
                Imgproc.approxPolyDP(correctFormatCnt, approxCurve, eps, true);

                if (cnt.size().height >= minPoints && cnt.size().height <= maxPoints)
                {
                    return new MatOfPoint(cnt.toArray());
                }
                if (approxCurve.size().height >= minPoints && approxCurve.size().height <= maxPoints)
                {
                    return new MatOfPoint(approxCurve.toArray());
                }
            }
            return new MatOfPoint();
        }

        // IF THIS FUNCTION IS SLOW, IMPLEMENT IT USING MAT INSTEAD OF int [,]
        internal static int [][] NonMaxSupressionFast(int [,] boxes, int overlapThresh)
        {
            // check if correct length is called
            if (boxes.GetLength(0) == 0) { return new int [1][]; }
            var pick = new List<int> { } ;
            var x1 = boxes.GetColumn(0);
            var y1 = boxes.GetColumn(1);
            var x2 = boxes.GetColumn(2);
            var y2 = boxes.GetColumn(3);
            var idxs = y2.ArgSort();

            // Compute area array
            var a1 = x2
                .Zip(x1, (el1, el2) => el1 - el2 + 1)
                .ToArray();
            var a2 = y2
                .Zip(y1, (el1, el2) => el1 - el2 + 1)
                .ToArray();
            var area = a1
                .Zip(a2, (el1, el2) => el1 * el2)
                .ToArray();

            while (idxs.Length > 0)
            {
                var lastIdx = idxs.Length - 1;
                var i = idxs[lastIdx];
                pick.Add(i);

                var idxArr = idxs.Take(lastIdx).ToArray();
                var xx1 = x1.GetValuesBy(idxArr).Maximum(new[] {x1[i]});
                var yy1 = y1.GetValuesBy(idxArr).Maximum(new[] {y1[i]});
                var xx2 = x2.GetValuesBy(idxArr).Maximum(new[] {x2[i]});
                var yy2 = y2.GetValuesBy(idxArr).Maximum(new[] {y2[i]});

                var w = xx2
                    .Zip(xx1, (el1, el2) => new {el1, el2})
                    .Select(pair => pair.el1 - pair.el2 + 1)
                    .ToArray()
                    .Maximum(new[] {0});
                var h = yy2
                    .Zip(yy1, (el1, el2) => new {el1, el2})
                    .Select(pair => pair.el1 - pair.el2 + 1)
                    .ToArray()
                    .Maximum(new[] {0});

                var wh = w
                    .Zip(h, (el1, el2) => (double)el1 * el2)
                    .ToArray();
                var overlap = area.GetValuesBy(idxArr)
                    .Zip(wh, (el1, el2) => el2 / el1);

                var indexesToDelete = overlap
                    .Select((x, i) => new KeyValuePair<double, int>(x, i))
                    .Where(el => el.Key > overlapThresh)
                    .Select(el => el.Value)
                    .ToArray();

                idxs = idxs
                    .Select((x, idx) => new KeyValuePair<int, int>(x, idx))
                    .Where(el => !indexesToDelete.Contains(el.Key))
                    .Select(el => el.Value)
                    .ToArray();
            }

            return pick
                .Select(i => boxes.GetRow(i))
                .ToArray();
        }
    }
}
