﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.ImgprocModule;

namespace CVModule
{
    internal static class Preprocessing
    {
        internal static Mat RgbToGray(Mat srcImgBgr)
        {
            Mat gray = new Mat();
            Imgproc.cvtColor(srcImgBgr, gray, Imgproc.COLOR_RGB2GRAY);
            return gray;
        }

        internal static Mat RgbToHsv(Mat srcImgBgr)
        {
            Mat hsv = new Mat();
            Imgproc.cvtColor(srcImgBgr, hsv, Imgproc.COLOR_RGB2HSV);
            return hsv;
        }

        internal static Mat GrayToBgr(Mat srcImgGray)
        {
            Mat bgrImg = new Mat();
            Imgproc.cvtColor(srcImgGray, bgrImg, Imgproc.COLOR_GRAY2BGR);
            return bgrImg;
        }

        internal static Mat Negative(Mat binImg)
        {
            Mat negative = new Mat();
            var scalar = new Scalar(255);
            Mat white = new Mat(size: binImg.size(), type: CvType.CV_8UC1, scalar);
            Core.subtract(white, binImg, negative);
            return negative;
        }

        internal static Point GetCenter(MatOfPoint contour)
        {
            Moments m = Imgproc.moments(contour);
            double cX = (m.m10 / m.m00);
            double cY = (m.m01 / m.m00);

            return new Point(cX, cY);
        }

        internal static Mat Crop(Mat img, int tlX, int tlY, int brX, int brY)
        {
            Point tlPoint = new Point(tlX, tlY);
            Point brPoint = new Point(brX, brY);
            return Crop(img, tlPoint, brPoint);
        }
        
        internal static Mat Crop(Mat img, Point tl, Point br)
        {
            Rect myROI = new Rect(tl, br);
            Mat cropped = new Mat(img, myROI);
            return cropped;
        }

        internal static List<Point> OrderPoints(List<Point> points)
        {
            if (points.Count != 4)
            {
                return points;
            }
            var sortedByX = points.OrderBy(pt => pt.x).ToList();
            
            var mostLeftPts = new List<Point> { sortedByX[0], sortedByX[1] };
            var mostRightPts = new List<Point> { sortedByX[2], sortedByX[3] };
            
            var sortedByYMostLeft = mostLeftPts.OrderBy(pt => pt.y).ToList();
            var sortedByYMostRight = mostRightPts.OrderBy(pt => pt.y).ToList();

            // Log.Add("Ordered points: " + string.Join("\t", new List<Point> { sortedByYMostLeft[0], sortedByYMostRight[0], sortedByYMostLeft[1], sortedByYMostRight[1] }));
            
            return new List<Point> { sortedByYMostLeft[1], sortedByYMostRight[1], sortedByYMostLeft[0], sortedByYMostRight[0] };
        }

        internal static List<Point> OrderPointsTb(MatOfPoint cnt)
        {
            var points = cnt.toList();
            var orderedPoints = OrderPoints(points);
            
            var tl = orderedPoints[2];
            var tr = orderedPoints[3];
            var bl = orderedPoints[0];
            var br = orderedPoints[1];

            return new List<Point> { tl, tr, bl, br };
        }

        internal static Mat CropContour(Mat img, MatOfPoint contour)
        {
            var rect = Imgproc.boundingRect(contour);
            return Crop(img, rect.tl(), rect.br());
        }

        internal static Mat Rotate(Mat img, double angle, Point imgCenter = null) 
        {
            if (imgCenter == null)
            {
                imgCenter = new Point((double)img.width()/2, (double)img.height()/2);
            }

            var rotMat = Imgproc.getRotationMatrix2D(imgCenter, angle, 1.0);
            var result = new Mat();
            Imgproc.warpAffine(img, result, rotMat, new Size(img.width(), img.height()), Imgproc.INTER_LINEAR);
            return result;
        }
    }
}
