﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.ImgprocModule;

namespace CVModule
{
    internal class Segmentation
    {
        internal static Mat Color(Mat rgbImg, Color c)
        {
            Mat destMask = new Mat();
            Mat hsv = Preprocessing.RgbToHsv(rgbImg);
            List<Scalar> bounds = c.BoundsValue();

            if (c == CVModule.Color.Red)
            {
                Mat redMask1 = new Mat();
                Mat redMask2 = new Mat();
                Core.inRange(hsv, bounds[0], bounds[1], redMask1);
                Core.inRange(hsv, bounds[2], bounds[3], redMask2);
                Core.bitwise_or(redMask1, redMask2, destMask);
            }
            else
            {
                Core.inRange(hsv, bounds[0], bounds[1], destMask);
            }
            
            return destMask;
        }

        internal static Mat AutoThreshold(Mat img)
        {
            Mat destMat = new Mat();
            Imgproc.threshold(img, destMat, 0, 255, Imgproc.THRESH_BINARY + Imgproc.THRESH_OTSU);
            return destMat;        
        }

        internal static Mat TwoThresholds(Mat img, int lowerBound = 0, int upperBound = 0)
        {
            Mat destMat = new Mat();
            var lowerScalar = new Scalar(Math.Min(lowerBound, upperBound));
            var upperScalar = new Scalar(Math.Max(lowerBound, upperBound));
            Core.inRange(img,lowerScalar, upperScalar, destMat);
            return destMat;
        }
    }
}
