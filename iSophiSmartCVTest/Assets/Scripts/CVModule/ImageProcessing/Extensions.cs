using System;
using System.Collections.Generic;
using System.Linq;

namespace CVModule
{
    internal static class ArrayExtensions
    {
        internal static T[] GetColumn<T>(this T[,] matrix, int column)
        {
            return Enumerable.Range(0, matrix.GetLength(0))
                .Select(x => matrix[x, column])
                .ToArray();
        }
        
        internal static T[] GetRow<T>(this T[,] matrix, int row)
        {
            return Enumerable.Range(0, matrix.GetLength(1))
                .Select(x => matrix[row, x])
                .ToArray();
        }
        
        internal static int[] ArgSort<T>(this T[] lst, bool asc = true)
        {
            var sorted = lst
                .Select((x, i) => new KeyValuePair<T, int>(x, i))
                .OrderBy(x => x.Key)
                .ToList();

            return asc
                ? sorted
                    .Select(x => x.Value)
                    .ToArray()
                : sorted
                    .Select(x => x.Value)
                    .Reverse()
                    .ToArray();
        }

        // Not a extension but it is related to arrays
        internal static T[] Maximum<T>(this T[] selfArr, T[] arr) where T : struct, IComparable<T>
        {
            if (arr.Length == 1)
            {
                var maxElArr = arr[0];
                return selfArr
                    .Select(el => Max(el, maxElArr))
                    .ToArray();
            }
            if (arr.Length == selfArr.Length)
            {
                return selfArr
                    .Zip(arr, (el1, el2) => new {el1, el2})
                    .Select(pair => Max(pair.el1, pair.el2))
                    .ToArray();
            }
            return selfArr;
        }
        
        internal static T[] Minimum<T>(this T[] selfArr, T[] arr) where T : struct, IComparable<T>
        {
            if (arr.Length == 1)
            {
                var maxElArr = arr[0];
                return selfArr
                    .Select(el => Min(el, maxElArr))
                    .ToArray();
            }
            if (arr.Length == selfArr.Length)
            {
                return selfArr
                    .Zip(arr, (el1, el2) => new {el1, el2})
                    .Select(pair => Min(pair.el1, pair.el2))
                    .ToArray();
            }
            return selfArr;
        }

        internal static T[] GetValuesBy<T>(this T[] selfArr, int[] indexes) where T : struct, IComparable<T>
        {
            return indexes
                .Select(i => selfArr[i])
                .ToArray();
        }

        internal static T Max<T>(T v1, T v2) where T : struct, IComparable<T>
        {
            return v1.CompareTo(v2) > 0 ? v1 : v2;
        }

        internal static T Min<T>(T v1, T v2) where T : struct, IComparable<T>
        {
            return v1.CompareTo(v2) > 0 ? v2 : v1;
        }
    }
}