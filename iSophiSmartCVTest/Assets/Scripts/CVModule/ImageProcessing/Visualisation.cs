
using System;
using System.Collections.Generic;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.ImgprocModule;

namespace CVModule
{
    internal class Visualisation 
    {
        internal static Mat DrawGrid(Mat image, AreaType type, int thickness, Scalar color, Tuple<int, int> imgSize, int shift = 0)
        {
            List<Point> gridCoordsV;
            List<Point> gridCoordsH;

            switch (type)
            {
                case AreaType.Grid4X4:
                    gridCoordsV = new List<Point>
                    {
                        new Point(115, 115),
                        new Point(495, 495),
                        new Point(874, 880),
                        new Point(1254, 1260),
                        new Point(1620, 1623),
                    };
                    gridCoordsH = new List<Point>
                    {
                        new Point(135  + shift, 140  + shift),
                        new Point(510  + shift, 505  + shift),
                        new Point(900  + shift, 895  + shift),
                        new Point(1290 + shift, 1288 + shift),
                        new Point(1680 + shift, 1680 + shift),
                    };
                    break;
                default: 
                    throw new InvalidCastException("Not implemented yet");
            }
            foreach (var coord in gridCoordsV)
            {
                var p1 = new Point(coord.x, 0);
                var p2 = new Point(coord.y, imgSize.Item2);
                Imgproc.line(image, p1, p2, color, thickness);
            }

            foreach (var coord in gridCoordsH)
            {
                var p1 = new Point(0, coord.x);
                var p2 = new Point(imgSize.Item1, coord.y);
                Imgproc.line(image, p1, p2, color, thickness);
            }

            Log.AddImage(image);
            return image;
        }
    }    
}