﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using OpenCVForUnity.CoreModule;

namespace CVModule
{
    internal static class Utilities
    {
        internal static int ToIntensity(int hueVal)
        {
            return (int)(hueVal * 0.5);
        }

        internal static double NormVec2(Vector2 vec)
        {
            return Math.Sqrt(Math.Pow(vec.X, 2) + Math.Pow(vec.Y, 2));
        }
    }
}
