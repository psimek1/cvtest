using OpenCVForUnity.CoreModule;
using OpenCVForUnity.ImgprocModule;

namespace CVModule
{
    internal class Filtration
    {
        internal static Mat Gauss(Mat img, int kernelSize = 3, int sigmaX = 0)
        {
            var dest = new Mat();
            Imgproc.GaussianBlur(img, dest, new Size(kernelSize, kernelSize), sigmaX);
            return dest;
        } 
        
        internal static Mat Median(Mat img, int ksize = 3)
        {
            var dest = new Mat();
            Imgproc.medianBlur(img, dest, ksize);
            return dest;
        }
    }
}