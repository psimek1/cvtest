using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.ImgprocModule;
using OpenCVForUnity.Calib3dModule;

namespace CVModule
{
    internal class Triangle
    {
        private static List<MatOfPoint> apprxCntrs;
        private static (bool, int?) _incorrectOut = (false, null);

        internal static List<Tile> FindTriangles(Mat binImg, Color c, Area a)
        {
            apprxCntrs = new List<MatOfPoint>();
            var foundTiles = new List<Tile>();
            var clearedBinImg = new Mat();
            var kernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(15, 15));
            Imgproc.morphologyEx(binImg, clearedBinImg, Imgproc.MORPH_OPEN, kernel);

            // Log.AddImage(clearedBinImg);
            
            var contours = Contours.MyFindContours(clearedBinImg, 100000 / 2, 160000 / 2);
            
            foreach (var cnt in contours)
            {
                (var isTriangle, var rotation) = CheckTriangCharacteristics(cnt, a.offset);
                
                if (isTriangle)
                {
                    var center = Preprocessing.GetCenter(cnt);
                    //Tile((cx, cy), ((cx - self.offset)/TILESIZE,(cy - self.offset)/TILESIZE), rotation, TileType.TRIANGLE, color)
                    var gridIndexes = new Tuple<int, int>(
                        (int) (center.x - a.offset) / Constants.TILESIZE,
                        (int) (center.y - a.offset) / Constants.TILESIZE
                    );
                    foundTiles.Add(new Tile(TileType.Diag, c, gridIndexes, center, rotation));
                }
            }
            
            // Contours.Draw(Preprocessing.GrayToBgr(binImg), apprxCntrs);
            
            return foundTiles;
        }

        private static (bool, int?) CheckTriangCharacteristics(MatOfPoint cnt, int offset)
        {
            var rect = Imgproc.boundingRect(cnt);
            if (Math.Abs(rect.width - rect.height) > 50)
            {
                return _incorrectOut;
            }

            var apxCnt = Contours.ApproxContour(cnt, 3, 3);
            if (apxCnt.empty())
            {
                return _incorrectOut;
            }
            
            apprxCntrs.Add(apxCnt);

            var center = Preprocessing.GetCenter(apxCnt);
            var points = OrderPointsTriang(apxCnt.toList());
            var rotation = FindRotation(points);

            var xIndex = (int) (center.x - offset) / Constants.TILESIZE;
            var yIndex = (int) (center.y - offset) / Constants.TILESIZE;

            return !IsFittingGrid(points, xIndex, yIndex, offset) ? _incorrectOut : (true, rotation);
        }

        private static int FindRotation([ItemCanBeNull] List<Point> points)
        {
            // 4 - bottom-right
            if (points[0] == null)
            {
                return 4;
            }

            // 3 - bottom-left
            if (points[1] == null)
            {
                return 3;
            }

            // 2 - top-right
            if (points[2] == null)
            {
                return 2;
            }

            // 1 - top-left
            if (points[3] == null)
            {
                return 1;
            }

            throw new CvException("No null in points!!");
        }

        [ItemCanBeNull]
        private static List<Point> OrderPointsTriang(List<Point> points)
        {
            if (points.Count != 3)
            {
                throw new CvException(nameof(points), points, null);
            }

            // Top-left, Top-right, Bottom-left, Bottom-right
            var sortedByX = points.OrderBy(pt => pt.x).ToList();
            var mostLeftPt = sortedByX[0];
            var midPt = sortedByX[1];
            var mostRightPt = sortedByX[2];

            if (Math.Abs(mostLeftPt.y - mostRightPt.y) < 20)
            {
                if (mostLeftPt.y < midPt.y)
                {
                    return Math.Abs(mostLeftPt.x - midPt.x) < 20
                        ? new List<Point> {mostLeftPt, mostRightPt, midPt, null}
                        : new List<Point> {mostLeftPt, mostRightPt, null, midPt};
                }

                return Math.Abs(mostLeftPt.x - midPt.x) < 20
                    ? new List<Point> {midPt, null, mostLeftPt, mostRightPt}
                    : new List<Point> {null, midPt, mostLeftPt, mostRightPt};
            }

            if (mostLeftPt.y < mostRightPt.y)
            {
                return Math.Abs(mostLeftPt.x - midPt.x) < 20
                    ? new List<Point> {mostLeftPt, null, midPt, mostRightPt}
                    : new List<Point> {mostLeftPt, midPt, null, mostRightPt};
            }

            return Math.Abs(mostLeftPt.x - midPt.x) < 20
                ? new List<Point> {midPt, mostRightPt, mostLeftPt, null}
                : new List<Point> {null, mostRightPt, mostLeftPt, midPt};
        }

        private static bool IsFittingGrid(List<Point> points, int xIndex, int yIndex, int offset)
        {
            for (int i = 0; i < points.Count; i += 2)
            {
                var bottom = i / 2;
                var p1 = points[i];
                var p2 = points[i + 1];

                if (p1 == null)
                {
                    // has no effect on the evaluation of if clause, will always be < 70
                    p1 = new Point(
                        offset + Constants.TILESIZE * xIndex,
                        offset + Constants.TILESIZE * (yIndex + bottom));
                }

                if (p2 == null)
                {
                    // has no effect on the evaluation of if clause, will always be < 70
                    p2 = new Point(
                        offset + Constants.TILESIZE * (xIndex + 1),
                        offset + Constants.TILESIZE * (yIndex + bottom));
                }

                // Top/Bottom left - to avoid rotations, moves
                // abs( x - (self.offset + TILESIZE * x_index)) > 70 or
                // abs( y - (self.offset + TILESIZE *(y_index + bottom))) > 70
                if (Math.Abs(p1.x - (offset + Constants.TILESIZE * xIndex)) > Constants.FittingGridToler ||
                    Math.Abs(p1.y - (offset + Constants.TILESIZE * (yIndex + bottom))) > Constants.FittingGridToler)
                {
                    return false;
                }

                // Top/Bottom right - to avoid rotations, moves
                // abs( x - (self.offset + TILESIZE * (x_index + 1))) > 70 or
                // abs( y - (self.offset + TILESIZE * (y_index + bottom))) > 70
                if (Math.Abs(p2.x - (offset + Constants.TILESIZE * (xIndex + 1))) > Constants.FittingGridToler ||
                    Math.Abs(p2.y - (offset + Constants.TILESIZE * (yIndex + bottom))) > Constants.FittingGridToler)
                {
                    return false;
                }
            }
            return true;
        }
    }
}