using OpenCVForUnity.CoreModule;
using OpenCVForUnity.ImgprocModule;
using System;
using System.Collections.Generic;
using System.Numerics;
using Unity.Mathematics;
using UnityEditor;
using UnityEditorInternal;

namespace CVModule
{
    internal class Stripe
    {
        private static (bool, Tuple<int, int>, int) _incorrectOut = (false, new Tuple<int, int>(0,0), 0);
        
        internal static List<Tile> FindStripes(Mat binImg, Color c, Area a)
        {
            var foundTiles = new List<Tile>();
            var clearedBinImg = new Mat();
            var kernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(15, 15));
            Imgproc.morphologyEx(binImg, clearedBinImg, Imgproc.MORPH_OPEN, kernel);

            var contours = Contours.MyFindContours(clearedBinImg, minArea: 100000 / 2, maxArea: 160000 / 2);

            foreach (var cnt in contours)
            {
                (var isStripe, var tileIndexes, var rotation) = CheckStripeCharacteristics(cnt, a);
                if (isStripe)
                {
                    var apxCnt = Contours.ApproxContour(cnt, 4, 4);
                    var center = Preprocessing.GetCenter(apxCnt);
                    foundTiles.Add(new Tile(TileType.Stripe, c, tileIndexes, center, rotation));
                }
            }

            return foundTiles;
        }

        private static (bool, Tuple<int, int>, int) CheckStripeCharacteristics(MatOfPoint cnt, Area a)
        {
            
            var rect = Imgproc.boundingRect(cnt);
            if (Math.Abs(rect.width - rect.height) < 50) { return _incorrectOut; }
            
            var apxCnt = Contours.ApproxContour(cnt, 4, 4);
            if (apxCnt.empty()) { return _incorrectOut; }

            var center = Preprocessing.GetCenter(apxCnt);
            
            // top-left, top-right, bottom-left, bottom-right order
            var points = Preprocessing.OrderPointsTb(apxCnt);
            
            // rotation = 1 -> left vertical
            // rotation = 2 -> right vertical
            // rotation = 3 -> top horizontal
            // rotation = 4 -> bottom horizontal
            var rotation = 1; var xIndex = 0; var yIndex = 0; var xTileIndex = 0; var yTileIndex = 0;
         
            var vec1 = new Vector2((float) points[0].x, (float) points[0].y);
            var vec2 = new Vector2((float) points[1].x, (float) points[1].y);
            
            // Vertical stripe
            if (Utilities.NormVec2(Vector2.Subtract(vec2, vec1)) < Constants.TILESIZE / 1.9)
            {
                xIndex = (int) (center.x - a.offset) / (Constants.TILESIZE / 2);
                yIndex = (int) (center.y - a.offset) / Constants.TILESIZE;
                xTileIndex = xIndex / 2;
                yTileIndex = yIndex;
                
                // odd number, right vertical stripe, else left vertical stripe
                rotation = xIndex % 2 == 1 ? 2 : 1; 
            }
            // Horizontal stripe
            else
            {
                xIndex = (int) (center.x - a.offset) / Constants.TILESIZE;
                yIndex = (int) (center.y - a.offset) / Constants.TILESIZE / 2;
                xTileIndex = xIndex;
                yTileIndex = yIndex / 2;
                
                rotation = yIndex % 2 == 1 ? 4 : 3;
            }
            if (!IsFittingGrid(points, rotation, xIndex, yIndex, a.offset)) { return _incorrectOut; }
            return (true, new Tuple<int, int>(xTileIndex, yTileIndex), rotation);
        }

        private static bool IsFittingGrid(List<Point> points, int rotation, int xIndex, int yIndex, int offset)
        {
            // Vertical left
            if (rotation == 1)
            {
                // Top-left
                // abs(points[0][0] - self.offset - indexes[0]*TILESIZE/2) > 70 or
                // abs(points[0][1] - self.offset - indexes[1]*TILESIZE) > 70:
                if (Math.Abs(points[0].x - offset - xIndex * (Constants.TILESIZE / 2)) > Constants.FittingGridToler ||
                    Math.Abs(points[0].y - offset - yIndex * Constants.TILESIZE) > Constants.FittingGridToler)
                {
                    return false;
                }
                // Bottom-left
                // abs(points[2][0] - self.offset - indexes[0]*TILESIZE/2) > 70 or
                // abs(points[2][1] - self.offset - (indexes[1] + 1)*TILESIZE) > 70:
                if (Math.Abs(points[2].x - offset - xIndex * (Constants.TILESIZE / 2)) > Constants.FittingGridToler ||
                    Math.Abs(points[2].y - offset - (yIndex + 1) * Constants.TILESIZE) > Constants.FittingGridToler)
                {
                    return false;
                }
                // Top-right
                // abs(points[1][0] - self.offset - (indexes[0] + 1)*TILESIZE/2) > 70 or
                // abs(points[1][1] - self.offset - indexes[1]*TILESIZE) > 70:
                if (Math.Abs(points[1].x - offset - (xIndex + 1) * (Constants.TILESIZE / 2)) > Constants.FittingGridToler ||
                    Math.Abs(points[1].y - offset - yIndex * Constants.TILESIZE) > Constants.FittingGridToler)
                {
                    return false;
                }
                // Bottom-right
                // abs(points[3][0] - self.offset - (indexes[0] + 1)*TILESIZE/2) > 70 or
                // abs(points[3][1] - self.offset - (indexes[1] + 1)*TILESIZE) > 70:
                if (Math.Abs(points[3].x - offset - (xIndex + 1) * (Constants.TILESIZE / 2)) > Constants.FittingGridToler ||
                    Math.Abs(points[3].y - offset - (yIndex + 1) * Constants.TILESIZE) > Constants.FittingGridToler)
                {
                    return false;
                }
            }
            
            if (rotation == 2)
            {
                // Top-right
                // abs(points[1][0] - self.offset - (indexes[0] + 1)*TILESIZE/2) > 70 or
                // abs(points[1][1] - self.offset - indexes[1]*TILESIZE) > 70:
                if (Math.Abs(points[1].x - offset - (xIndex + 1) * (Constants.TILESIZE / 2)) > Constants.FittingGridToler ||
                    Math.Abs(points[1].y - offset - yIndex * Constants.TILESIZE) > Constants.FittingGridToler)
                {
                    return false;
                }
                // Bottom-right
                // abs(points[3][0] - self.offset - (indexes[0] + 1)*TILESIZE/2) > 70 or
                // abs(points[3][1] - self.offset - (indexes[1] + 1)*TILESIZE) > 70:
                if (Math.Abs(points[3].x - offset - (xIndex + 1) * (Constants.TILESIZE / 2)) > Constants.FittingGridToler ||
                    Math.Abs(points[3].y - offset - (yIndex + 1) * Constants.TILESIZE) > Constants.FittingGridToler)
                {
                    return false;
                }
                // Top-left
                // abs(points[0][0] - self.offset - indexes[0]*TILESIZE/2) > 70 or
                // abs(points[0][1] - self.offset - indexes[1]*TILESIZE) > 70:
                if (Math.Abs(points[0].x - offset - xIndex * (Constants.TILESIZE / 2)) > Constants.FittingGridToler ||
                    Math.Abs(points[0].y - offset - yIndex * Constants.TILESIZE) > Constants.FittingGridToler)
                {
                    return false;
                }
                // Bottom-left
                // abs(points[2][0] - self.offset - indexes[0]*TILESIZE/2) > 70 or
                // abs(points[2][1] - self.offset - (indexes[1] + 1)*TILESIZE) > 70:
                if (Math.Abs(points[2].x - offset - xIndex * (Constants.TILESIZE / 2)) > Constants.FittingGridToler ||
                    Math.Abs(points[2].y - offset - (yIndex + 1) * Constants.TILESIZE) > Constants.FittingGridToler)
                {
                    return false;
                }
            }

            if (rotation == 3)
            {
                // Top-left
                // abs(points[0][0] - self.offset - indexes[0]*TILESIZE) > 70 or
                // abs(points[0][1] - self.offset - indexes[1]*TILESIZE/2) > 70:
                if (Math.Abs(points[0].x - offset - xIndex * Constants.TILESIZE) > Constants.FittingGridToler ||
                    Math.Abs(points[0].y - offset - yIndex * (Constants.TILESIZE/2)) > Constants.FittingGridToler)
                {
                    return false;
                }
                
                // Bottom-left
                // abs(points[2][0] - self.offset - indexes[0]*TILESIZE) > 70 or
                // abs(points[2][1] - self.offset - (indexes[1] + 1)*TILESIZE/2) > 70:
                if (Math.Abs(points[2].x - offset - xIndex * Constants.TILESIZE) > Constants.FittingGridToler ||
                    Math.Abs(points[2].y - offset - (yIndex + 1) * (Constants.TILESIZE/2)) > Constants.FittingGridToler)
                {
                    return false;
                }
                
                // Top-right
                // abs(points[1][0] - self.offset - (indexes[0] + 1)*TILESIZE) > 70 or
                // abs(points[1][1] - self.offset - indexes[1]*TILESIZE/2) > 70:
                if (Math.Abs(points[1].x - offset - (xIndex + 1) * Constants.TILESIZE) > Constants.FittingGridToler ||
                    Math.Abs(points[1].y - offset - yIndex * (Constants.TILESIZE/2)) > Constants.FittingGridToler)
                {
                    return false;
                }
                
                // Bottom-right
                // abs(points[3][0] - self.offset - (indexes[0] + 1)*TILESIZE) > 70 or
                // abs(points[3][1] - self.offset - (indexes[1] + 1)*TILESIZE/2) > 70:
                if (Math.Abs(points[3].x - offset - (xIndex + 1) * Constants.TILESIZE) > Constants.FittingGridToler ||
                    Math.Abs(points[3].y - offset - (yIndex + 1) * (Constants.TILESIZE/2)) > Constants.FittingGridToler)
                {
                    return false;
                }
            }

            if (rotation == 4)
            {
                // Top-left
                // abs(points[0][0] - self.offset - indexes[0]*TILESIZE) > 70 or
                // abs(points[0][1] - self.offset - indexes[1]*TILESIZE/2) > 70:
                if (Math.Abs(points[0].x - offset - xIndex * Constants.TILESIZE) > Constants.FittingGridToler ||
                    Math.Abs(points[0].y - offset - yIndex * (Constants.TILESIZE/2)) > Constants.FittingGridToler)
                {
                    return false;
                }
                
                // Bottom-left
                // abs(points[2][0] - self.offset - indexes[0]*TILESIZE) > 70 or
                // abs(points[2][1] - self.offset - (indexes[1] + 1)*TILESIZE/2) > 70:
                if (Math.Abs(points[2].x - offset - xIndex * Constants.TILESIZE) > Constants.FittingGridToler ||
                    Math.Abs(points[2].y - offset - (yIndex + 1) * (Constants.TILESIZE/2)) > Constants.FittingGridToler)
                {
                    return false;
                }
                
                // Top-right
                // abs(points[1][0] - self.offset - (indexes[0] + 1)*TILESIZE) > 70 or
                // abs(points[1][1] - self.offset - indexes[1]*TILESIZE/2) > 70:
                if (Math.Abs(points[1].x - offset - (xIndex + 1) * Constants.TILESIZE) > Constants.FittingGridToler ||
                    Math.Abs(points[1].y - offset - yIndex * (Constants.TILESIZE/2)) > Constants.FittingGridToler)
                {
                    return false;
                }
                
                // Bottom-right
                // abs(points[3][0] - self.offset - (indexes[0] + 1)*TILESIZE) > 70 or
                // abs(points[3][1] - self.offset - (indexes[1] + 1)*TILESIZE/2) > 70:
                if (Math.Abs(points[3].x - offset - (xIndex + 1) * Constants.TILESIZE) > Constants.FittingGridToler ||
                    Math.Abs(points[3].y - offset - (yIndex + 1) * (Constants.TILESIZE/2)) > Constants.FittingGridToler)
                {
                    return false;
                }
            }

            return true;
        }
        
        

    }
}