using OpenCVForUnity.ImgprocModule;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using OpenCVForUnity.CoreModule;



namespace CVModule
{
    internal class Patterns
    {
        // TMP PROPERTY JUST TO IMITATE IMPLEMENTATION IN PYTHON
        private static List<PatternTile> patternTiles = new List<PatternTile>(); 
        
        static internal List<Tile> FindPatterns()
        {
            // TBD
        }
        
        internal static Mat SegmentationFindContours(Area area)
        {
            // Grid
            var avg = Core.mean(Preprocessing.RgbToGray(area.image));
            var img = Visualisation.DrawGrid(area.image, area.type, 60, avg, area.CroppedSize());
            var imgSize = area.CroppedSize();
            Imgproc.line(img, new Point(0, 0), new Point(imgSize.Item1, 0), avg, 200);
            Imgproc.line(img, new Point(0, 0), new Point(0, imgSize.Item2), avg, 200);
            
            // Segmentation, filtration of contours
            var gray = Preprocessing.RgbToGray(img);
            var negative = Preprocessing.Negative(Filtration.Median(gray, 5));
            var mask = Segmentation.AutoThreshold(negative);

            var cnts = Contours.MyFindContours(mask, 6000, 50000);

            var nCnts = new List<MatOfPoint>();
            foreach (var cnt in cnts)
            {
                var rect = Imgproc.boundingRect(cnt);
                if (
                    rect.width > 30 && rect.height > 30 && Imgproc.contourArea(cnt) > 12000 ||  // filtrace zbytků šumu na základě šířky a výšky, coord dílky jsou 30 a více px široké
                    rect.width < 100 && rect.height < 100 && Imgproc.contourArea(cnt) < 9000 || // přidání coords dílků
                    rect.width < 200 && rect.height < 200 && Imgproc.contourArea(cnt) < 12000)  // přidání rotate dílků
                {
                    nCnts.Add(cnt);
                }
            }
            // Morfological operations to find contours

            Imgproc.drawContours(new Mat(mask.size(), CvType.CV_8UC1, Scalar.all(0)), nCnts, -1,
                Scalar.all(255), Imgproc.FILLED);
            var strel59 = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(91, 91));
            var strel5 = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(11, 11));
            
            var tmpImg = new Mat();
            Imgproc.morphologyEx(mask, tmpImg, Imgproc.MORPH_OPEN, strel5);
            Imgproc.morphologyEx(tmpImg, mask, Imgproc.MORPH_CLOSE, strel59);

            cnts = Contours.MyFindContours(mask, 10000, 50000);
            var cntsFiltered = new List<MatOfPoint>();

            foreach (var cnt in cnts)
            {
                if (ImageFeatures.AspectRatio(Preprocessing.CropContour(mask, cnt)) > 0.5)
                {
                    var tile = new PatternTile(Imgproc.boundingRect(cnt), Imgproc.minAreaRect(new MatOfPoint2f(cnt)));
                    patternTiles.Add(tile);
                    cntsFiltered.Add(cnt);
                }
            }
            Log.Add("Contours found: " + cntsFiltered.Count);

            // Rest is some garbage, idk.
            
            return mask;
        }

        private static bool IsFittingGrid(PatternTile pattern, List<int> tileIndex, int offset)
        {
            var tileCenter = tileIndex
                .Select(el => el * Constants.TILESIZE + offset + Constants.TILESIZE / 2)
                .ToList();
            
            var vec1 = new Vector2(tileCenter[0], tileCenter[1]);
            var vec2 = new Vector2((float) pattern.minAreaRect.center.x, (float) pattern.minAreaRect.center.x);
            return !(Utilities.NormVec2(Vector2.Subtract(vec1, vec2)) > Constants.GridPositionTolerance);
        }

        private static (int, double) MatchPattern(Mat patternImg, int cntCount, PatternsDB db, PatternInfo patternInfo = null)
        {
            var possiblePatterns = db.db
                .Where(el => el.contoursCount == cntCount)
                .ToList();

            foreach (var val in possiblePatterns)
            {
                if (patternInfo != null && patternInfo.imgName == val.imgName)
                {
                    (var sH, var sW) = (patternImg.height(), patternImg.width());
                    foreach (var angle in Constants.Rotations.Take(Constants.Rotations.Count - 1))
                    {
                        var temp = Preprocessing.Rotate(val.patternImage, angle.Value());
                        var idx = new Mat();
                        Core.findNonZero(temp, idx);
                        var img0C = Preprocessing.CropContour(temp, new MatOfPoint(idx));
                        (var tH, var tW) = (img0C.height(), img0C.width());
                        (var cH, var cW) = tH > tW ? (sW, sH) : (sH, sW);
                        
                        var resizedImg0C = new Mat();
                        var resizedPatternC = new Mat();
                        Imgproc.resize(img0C, resizedImg0C, new Size(cH, cW), Imgproc.INTER_AREA);
                        Imgproc.resize(patternImg, resizedPatternC, new Size(cH, cW), Imgproc.INTER_AREA);

                        var result = new Mat();
                        Imgproc.matchTemplate(resizedPatternC, resizedImg0C, result, Imgproc.TM_CCOEFF_NORMED);
                        
                        // Why is taking coordinates of the result?
                        // What if the biggest number will be on the position different from 0?
                    }
                }
            }
        }
    }
}