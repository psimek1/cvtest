using OpenCVForUnity.CoreModule;
using OpenCVForUnity.ImgprocModule;
using System;
using System.Collections.Generic;

namespace CVModule
{
    internal class Solid
    {
        /// <summary>
        /// Finds solid rectangles on the grid
        /// </summary>
        /// <param name="binImg"></param>
        /// <param name="c"></param>
        /// <param name="a"></param>
        /// <returns></returns>
        internal static List<Tile> FindSolids(Mat binImg, Color c, Area a)
        {
            // Log.Add("Inside find solids");
            // Log.AddImage(binImg.clone());
            var foundTiles = new List<Tile>();
            var contours = Contours.MyFindContours(binImg, minArea: 100000, maxArea: 160000);

            foreach(var cnt in contours)
            {
                if (CheckSolidCharacteristics(cnt, a.offset))
                {
                    var approx = Contours.ApproxContour(cnt, minPoints: 4, maxPoints: 4);
                    var center = Preprocessing.GetCenter(approx);

                    var gridCoords = new Tuple<int, int>(
                        (int)Math.Floor((center.x - a.offset) / Constants.TILESIZE), 
                        (int)Math.Floor((center.y - a.offset) / Constants.TILESIZE));
                    
                    // Log.Add("Found square tile!");
                    
                    foundTiles.Add(new Tile(
                        tileType: TileType.Solid, color: c, gridCoords: gridCoords, imageCoords: center)
                    );
                }
            }
            return foundTiles;
        }

        /// <summary>
        /// Boolean value that checks if the tile is solid square
        /// </summary>
        /// <param name="cnt"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        private static bool CheckSolidCharacteristics(MatOfPoint cnt, int offset)
        {
            var r1 = Imgproc.boundingRect(cnt);
            if (Math.Abs(r1.width - r1.height) > 50) { return false; }

            var approxCnt = Contours.ApproxContour(cnt, minPoints: 4, maxPoints: 4);
            if (approxCnt.empty()) { return false; }

            var r2 = Imgproc.boundingRect(approxCnt);
            if (Math.Abs(r2.width - r2.height) > 50) { return false; }

            var center = Preprocessing.GetCenter(approxCnt);
            var xIndex = (int)(center.x / Constants.TILESIZE);
            var yIndex = (int)(center.y / Constants.TILESIZE);

            var points = Preprocessing.OrderPointsTb(approxCnt);
            // Log.Add("Ordered points " + string.Join("\t", points));

            return IsFittingGrid(points, xIndex, yIndex, offset);
        }

        private static bool IsFittingGrid(List<Point> points, int xIndex, int yIndex, int offset)
        {
            for (int i=0; i < points.Count; i+=2)
            {
                var bottom = i / 2;
                var p1 = points[i];         // top-left ,   bottom-left
                var p2 = points[i + 1];     // top-right,   bottom-right

                // Top/Bottom left - to avoid rotations, moves
                // Log.Add(Math.Abs(p1.x - (offset + Constants.TILESIZE * xIndex)) + "");
                // Log.Add(Math.Abs(p2.x - (offset + Constants.TILESIZE * (xIndex + 1))) + "");
                // Log.Add(Math.Abs(p2.y - (offset + Constants.TILESIZE * (yIndex + bottom))) + "");
                
                // Log.Add(Math.Abs(p1.y - (offset + Constants.TILESIZE*(yIndex + bottom))) + "");
                if (Math.Abs(p1.x - (offset + Constants.TILESIZE * xIndex)) > Constants.FittingGridToler ||
                    Math.Abs(p1.y - (offset + Constants.TILESIZE * (yIndex + bottom))) > Constants.FittingGridToler
                ) { return false; }
                
                // Top/Bottom right - to avoid rotations, moves
                if (Math.Abs(p2.x - (offset + Constants.TILESIZE * (xIndex + 1))) > Constants.FittingGridToler ||
                    Math.Abs(p2.y - (offset + Constants.TILESIZE * (yIndex + bottom))) > Constants.FittingGridToler
                ) { return false; }
            }

            return true;
        }
    }
}