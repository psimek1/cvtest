using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.ImgprocModule;


namespace CVModule
{
    internal class Calibration
    {
        internal static List<MatOfPoint> GetCalibContours(Mat calibImg)
        {
            var mask = Segmentation.TwoThresholds(Preprocessing.RgbToGray(calibImg), 0, 100);
            var contours = Contours.MyFindContours(
                mask,
                mask.rows() * mask.cols() / Constants.minCoef,
                mask.rows() * mask.cols() / Constants.maxCoef);

            var calibPtsContours = new List<MatOfPoint>();
            var minAreaRects = new List<Rect>();
            foreach (var cnt in contours)
            {
                var rect = Imgproc.boundingRect(cnt);
                if (Math.Abs(rect.width - rect.height) < Math.Min(rect.width, rect.height) * Constants.IMG_COEF)
                {
                    calibPtsContours.Add(cnt);
                    minAreaRects.Add(rect);
                }
            }
            
            var finalContours = new List<MatOfPoint>(); 
            if (calibPtsContours.Count > 4)
            {
                // enumerate and loop through contours
                foreach (var it in minAreaRects.Select((x, i) => new { Value = x, Index = i }) )
                {
                    var rect1 = it.Value;
                    var thresh1 = (rect1.width + rect1.height) / 2;
                    
                    foreach (var rect2 in minAreaRects)
                    {
                        var thresh2 = (rect2.height + rect2.width) / 2;
                        if ((rect1 != rect2) &&
                            Math.Abs(rect1.y - rect2.y) < Math.Min(thresh1, thresh2) * 1.5)
                        {
                            finalContours.Add(calibPtsContours[it.Index]);
                            break;
                        }
                    }
                }
            }
            finalContours = calibPtsContours;
            if (finalContours.Count != 4)
            {
                throw new InvalidProgramException("Ayy man, where my 4 points at? Now I have " + finalContours.Count + " points...");
            }
            // Contours.Draw(calibImg, finalContours);
            return finalContours;
        }

        internal static Point FindMark(Mat img)
        {
            var mask = Segmentation.TwoThresholds(Preprocessing.RgbToGray(img), 0, 100);
            var contours = Contours.MyFindContours(mask, 70000, 85000);

            Log.Add("Rows: " + img.rows() + "Cols: " + img.cols());
            
            var markCoords = new Point();
            foreach (var cnt in contours)
            {
                var rect = Imgproc.boundingRect(cnt);
                //  && Math.Abs(rect.x - Constants.REF_MARK_COORD[0]) < 400
                if ((double) rect.width / rect.height < 360.0 / 160.0 * 1.1 && 
                    Math.Abs(rect.x - 1.0/2.0*Constants.RefWarpedImgWidth) < Constants.MarkToleration &&    // check if contour is horizontaly in the middle 
                    Math.Abs(rect.y - 3.0/5.0*Constants.RefWarpedImgHeight) < Constants.MarkToleration)     // check if contour is verticaly in the middle 
                {
                    markCoords = Preprocessing.GetCenter(cnt);
                    Contours.Draw(img, new List<MatOfPoint>{cnt});
                    break;
                }
            }

            return markCoords;
        }
    }
}