using System;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.ImgprocModule;

namespace CVModule
{
    class ShapeDescriptor
    {
        internal static double AspectRatio(double minDiameter, double maxDiameter)
        {
            return minDiameter / maxDiameter;
        }
    }
    
    internal class ImageFeatures
    {
        internal static double AspectRatio(Mat binImg)
        {
            var contours = Contours.MyFindContours(binImg);
            var minAreaRect = Imgproc.minAreaRect(new MatOfPoint2f(contours[0]));
            var maxDiameter = Math.Max(minAreaRect.size.height, minAreaRect.size.width);
            var minDiameter = Math.Min(minAreaRect.size.height, minAreaRect.size.width);
            return ShapeDescriptor.AspectRatio(maxDiameter, minDiameter);
        }
    }
}