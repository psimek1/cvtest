﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVModule
{
    public class TileInfo
    {
        internal TileInfo(TileInfo x)
        {
            name = x.name;
            maxCount = x.maxCount;
            canOverlay = x.canOverlay;
            cvTile = x.cvTile;
            type = x.type;
            color = x.color;
        }

        internal TileInfo(string name, int maxCount, List<string> canOverlay)
        {
            this.name = name;
            this.maxCount = maxCount;
            this.canOverlay = canOverlay;
            
            cvTile = name.GetCvTile();
            (type, color) = cvTile.TypeAndColor();
        }

        internal string name { get; private set; }
        internal int maxCount { get; private set; }
        internal List<string> canOverlay { get; private set; }
        
        internal CvTile cvTile { get; private set; }
        internal TileType type { get; private set; }
        internal Color color { get; private set; }
    }
}
