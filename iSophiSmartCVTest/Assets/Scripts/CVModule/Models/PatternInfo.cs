using System;
using System.Collections.Generic;
using OpenCVForUnity.CoreModule;

namespace CVModule
{
    internal class PatternInfo
    {
        internal PatternInfo(string imgName, int contoursCount, List<double> features, Mat patternImage)
        {
            this.imgName = imgName;
            this.contoursCount = contoursCount;
            this.features = features;
            this.patternImage = patternImage;
        }

        internal string imgName { get; private set; }
        internal int contoursCount { get; private set; }
        internal List<Double> features { get; private set; }
        internal Mat patternImage { get; private set; }
    }
}