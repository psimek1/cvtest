﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.ImgprocModule;
using UnityEngine.UIElements;

namespace CVModule
{
    public class Area
    {
        public Area(AreaType type, List<CvTile> objects)
        {
            this.type = type;
            this.objects = new List<TileInfo>();
            objectsEnums = objects;
            SetupArea();
        }

        internal AreaType type { get; }
        internal List<TileInfo> objects { get; private set; }
        internal List<CvTile> objectsEnums { get; }
        
        // internal helpers
        internal Tuple<Point, Point> coords { get; private set; } // top left and bottom right points of area
        internal Tuple<Point, Point> correctedCoords { get; private set; } // top left and bottom right points of area
        internal int offset { get; private set; }           // size of grid's border in px
        internal Tuple<int, int> size { get; private set; } // size of the grid in tiles
        internal Mat image { get; private set; }            // cropped image of area

        internal void SetupArea()
        {
            if (type == AreaType.Grid4X4)
            {
                coords = new Tuple<Point, Point> (
                    new Point(925, 545),
                    new Point(2605, 2250)
                );
                size = new Tuple<int, int>(4, 4);
                offset = 120;
            }
            else if (type == AreaType.Grid8X2)
            {
                coords = new Tuple<Point, Point>(
                    new Point(1010, 550),
                    new Point(2745, 2360)
                );
                size = new Tuple<int, int>(4, 4);
                offset = 120;
            }
            else
            {
                throw new InvalidProgramException("Cannot finish constructing Area object!");
            }
        }

        internal void MapObjects(TilesDB database)
        {
            foreach (KeyValuePair<string, List<TileInfo>> entry in database.db) 
            {
                if ( entry.Key == type.GetString() ) 
                {
                    foreach (TileInfo tile in entry.Value)
                    {
                        if ( objectsEnums.Contains(tile.name.GetCvTile()) )
                        {
                            objects.Add(new TileInfo(tile));
                        }
                    }
                    break;
                }
            }
        }

        internal void Calibrate( List<double> dif, Mat img )
        {
            // Updates coordinates
            var topLeft = new Point ( (coords.Item1.x - dif[0]), (coords.Item1.y - dif[1]));
            var bottomRight = new Point ( (coords.Item2.x - dif[0]), (coords.Item2.y - dif[1]));
            correctedCoords = new Tuple<Point, Point> (topLeft, bottomRight);
            
            // Crop image
            image = Preprocessing.Crop(img, (int)topLeft.x, (int)topLeft.y, (int)bottomRight.x, (int)bottomRight.y);
            // Log.AddImage(image);
            
            // draw grid
            if ( type.IsGrid() )
            {
                image = Visualisation.DrawGrid(image, type, 16, Color.Black.Value(), CroppedSize());
            }
        }

        internal Tuple<int, int> CroppedSize()
        {
            var w = correctedCoords.Item2.x - correctedCoords.Item1.x;
            var h = correctedCoords.Item2.y - correctedCoords.Item1.y;
            return new Tuple<int, int>((int)w, (int)h);
        }
    }
}
