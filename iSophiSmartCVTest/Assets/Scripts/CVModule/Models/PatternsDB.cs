using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.UnityUtils;

namespace CVModule
{
    public class PatternsDB
    {
        internal PatternsDB()
        {
            db = new List<PatternInfo>();
        }
        internal PatternsDB(List<PatternInfo> db)
        {
            this.db = db;
        }

        internal List<PatternInfo> db { get; private set; }

        internal void ParseDBFromJson(Action onLoad)
        {
            ResourcesUtils.LoadText(ResourcesUtils.ResourcesMode.BuiltInResources, "PatternsDB", text =>
            {
                if (text == null)
                {
                    throw new InvalidProgramException("Could not load TilesDB.");
                }
                RootPatterns parsedJson = JsonUtility.FromJson<RootPatterns>(text);

                foreach (var v in parsedJson.values)
                {
                    ResourcesUtils.LoadImage(ResourcesUtils.ResourcesMode.BuiltInResources, "PatternsImages/" + v.imgName, texture =>
                    {
                        var img = new Mat(texture.height, texture.width, CvType.CV_8UC3);
                        Utils.texture2DToMat(texture, img);
                        var pattern = new PatternInfo(v.imgName, v.cntAmount, new List<double>(v.features), img);
                        db.Add(pattern);
                    });
                }
                onLoad();
            });
        }
    }
    
    [System.Serializable]
    class ValuePatterns
    {
        public int cntAmount;
        public string imgName;
        public double[] features;
    }
    [System.Serializable]
    class RootPatterns
    {
        public ValuePatterns[] values;
    }
}