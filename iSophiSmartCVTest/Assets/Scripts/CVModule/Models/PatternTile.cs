using System;
using System.Collections.Generic;
using System.Linq;
using OpenCVForUnity.ImgprocModule;
using OpenCVForUnity.CoreModule;
using UnityEngine;
using Rect = OpenCVForUnity.CoreModule.Rect;

namespace CVModule
{
    internal class PatternTile
    {
        internal PatternTile(Rect boundingRect, RotatedRect minAreaRect)
        {
            rect = boundingRect;
            pattern = new Mat();
        }

        internal void Inject(Rotation rotationEnum)
        {
            rotation = rotationEnum;
        }
        
        internal Rect rect { get; private set; }
        internal RotatedRect minAreaRect { get; private set; }
        internal Mat pattern { get; private set; }
        internal Rotation rotation { get; private set; }
        
        

        internal void CropToPattern (Mat img)
        {
            var localPattern = Preprocessing.Crop(img, rect.x - Constants.PatternCrop, rect.y - Constants.PatternCrop,
                (int) rect.br().x + Constants.PatternCrop, (int) rect.br().y + Constants.PatternCrop);
            localPattern = Preprocessing.Rotate(localPattern, minAreaRect.angle);
            var nonZeroIdxs = new Mat();
            Core.findNonZero(localPattern, nonZeroIdxs);
            pattern = Preprocessing.CropContour(localPattern, new MatOfPoint(nonZeroIdxs));
        }

        internal bool ResolveRotation()
        {
            var angle = rotation.Value() - minAreaRect.angle >= 0
                ? rotation.Value() - minAreaRect.angle
                : rotation.Value() - minAreaRect.angle + 360;
            
            var minAngleAndIndex = Constants.Rotations
                .Select((x, i) => new KeyValuePair<Rotation, int>(x, i))
                .OrderBy(x => x.Key)
                .ToList()[0];
            (var closestDif, var closestIndex) = (minAngleAndIndex.Key, minAngleAndIndex.Value); 

            if (closestDif.Value() < Constants.PatternAngleTolerance)
            {
                if (closestIndex == 4) { closestIndex = 0; }
                rotation = Constants.Rotations[closestIndex];
                return true;
            }
            return false;
        }
    }
}