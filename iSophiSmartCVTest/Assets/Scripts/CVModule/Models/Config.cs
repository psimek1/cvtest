﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCVForUnity.CoreModule;

namespace CVModule
{
    public class Config
    {
        public Config(List<Area> areas, Dictionary<string, string> parameters)
        {
            this.areas = areas;
            this.parameters = parameters;
        }

        internal List<Area> areas { get; private set; }
        internal Dictionary<string, string> parameters { get; private set; }

        internal void MapObjects(TilesDB db)
        {
            foreach (Area a in areas)
            {
                a.MapObjects(db);
            }
        }

        internal void CalibrateAreas(List<double> dif, Mat img)
        {
            foreach (Area a in areas)
            {
                a.Calibrate(dif, img.clone());
            }
        }
    }
}
