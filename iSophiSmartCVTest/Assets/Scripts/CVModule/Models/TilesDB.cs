﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CVModule
{
    public class TilesDB
    {
        internal TilesDB()
        {
            db = new Dictionary<string, List<TileInfo>>();
        }
        internal TilesDB(Dictionary<string, List<TileInfo>> db)
        {
            this.db = db;
        }

        internal Dictionary<string, List<TileInfo>> db { get; private set; }

        internal void ParseDBfromJson(Action onLoad)
        {
            ResourcesUtils.LoadText(ResourcesUtils.ResourcesMode.BuiltInResources, "TilesDB", text =>
            {
                if (text == null)
                {
                    throw new InvalidProgramException("Could not load TilesDB.");
                }
                Root parsedJson = JsonUtility.FromJson<Root>(text);

                foreach (Value value in parsedJson.values)
                {
                    AddIntoDB(value.areaName, value.tiles);
                }
                onLoad();
            });
        }

        void AddIntoDB(string key, TileFromJson[] tiles)
        {
            var newTiles = new List<TileInfo>();
            foreach (TileFromJson tile in tiles)
            {
                List<string> canOverlay = new List<string>(tile.canOverlay);
                TileInfo newTile = new TileInfo(tile.name, tile.maxAmount, canOverlay);
                newTiles.Add(newTile);
            }
            db.Add(key, newTiles);
        }
    }

    [System.Serializable]
    class TileFromJson
    {
        public string name;
        public int maxAmount;
        public string[] canOverlay;
    }
    [System.Serializable]
    class Value
    {
        public string areaName;
        public TileFromJson[] tiles;
    }
    [System.Serializable]
    class Root
    {
        public Value[] values;
    }
}
