﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCVForUnity.CoreModule;

namespace CVModule
{
    public class Tile
    {
        internal Tile(TileType tileType, Color color, Tuple<int, int> gridCoords, Point imageCoords, int? rotation = null)
        {
            type = tileType;
            this.color = color;
            this.gridCoords = gridCoords;
            this.imageCoords = imageCoords;
            this.rotation = rotation;
            cvTile = (type.GetString() + color.GetString()).GetCvTile();
        }

        internal string Print()
        {
            return this.type + "\t" + this.color.ToString() + "\t" + this.gridCoords;
        }

        internal CvTile cvTile { get; private set; }
        internal TileType type { get; private set; }
        internal Color color { get; private set; }
        internal Tuple<int, int> gridCoords { get; private set; }
        internal Point imageCoords { get; private set; }
        internal int? rotation { get; private set; }
    }
}
