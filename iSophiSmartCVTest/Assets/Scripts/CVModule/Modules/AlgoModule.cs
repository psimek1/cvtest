﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.UnityUtils;
using OpenCVForUnity.ImgprocModule;


namespace CVModule
{
    public class AlgoModule: Module
    { 
        public AlgoModule(Config config, Mat calibImg) : base(config, calibImg) 
        { }

        public override Dictionary<AreaType, List<Tile>> GetObjectsInfo(Mat img)
        {
            Log.Add("----- START -----");
            // clear found tiles (until circles on top of parts will be added)
            foundTiles.Clear();
            CalibrateImage(img);
            
            foreach (var a in config.areas)
            {
                // Log.Add(a.type.GetString());
                var detectedTiles = new List<Tile> {};
                foreach (var obj in a.objects)
                {
                    switch (obj.type)
                    {
                        case TileType.Solid:
                            detectedTiles.AddRange(
                                Solid.FindSolids(
                                    Segmentation.Color(a.image, obj.color),
                                    obj.color, a
                                    ));
                            break;
                        case TileType.Stripe:
                            detectedTiles.AddRange(
                                Stripe.FindStripes(
                                    Segmentation.Color(a.image, obj.color), 
                                    obj.color, a
                                    ));
                            break;
                        case TileType.Diag:
                            Log.Add(obj.type.GetString() + "\t_\t" + obj.color.GetString());
                            detectedTiles.AddRange(
                                Triangle.FindTriangles(
                                    Segmentation.Color(a.image, obj.color),
                                    obj.color, a
                                    ));
                            break;
                        default:
                            break;
                    }
                    // Log.Add(detectedTiles.Count.ToString());
                }
                foundTiles.Add(a.type, detectedTiles);
            }
            
            Log.Add("------ END ------");

            return foundTiles;
        }
    }
}
