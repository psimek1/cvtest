﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.ImgprocModule;
using OpenCVForUnity.Calib3dModule;

namespace CVModule
{
    public abstract class Module
    {
        protected Mat calibImage;
        protected Mat image;                // image that is capcuter every second
        protected Mat H;                    // transformation matrix for image correction and calib.
        protected Point markCoords;         // coordinates of middle mark for calibration
        protected Dictionary<AreaType, List<Tile>> foundTiles;  // array of found tiles

        protected Config config;
        protected TilesDB db;
        protected PatternsDB patternsDB;

        public bool failedInit = false;

        // jsonWorker

        protected Module(Config config, Mat calibImg)
        {
            image = new Mat();
            db = new TilesDB();
            patternsDB = new PatternsDB();
            foundTiles = new Dictionary<AreaType, List<Tile>>();
            calibImage = calibImg;
            
            // sequentional load.
            // could be parallel (find DispatchGroup equivalent)
            db.ParseDBfromJson(() => {
                patternsDB.ParseDBFromJson(() => {
                    
                    this.config = config;
                    config.MapObjects(db);
                    InitCalibration();
                });
            });
        }

        private void InitCalibration()
        {
            var calibPtsContours =  Calibration.GetCalibContours(calibImage);

            var centers = new List<Point>();
            foreach (var cnt in calibPtsContours)
            {
                centers.Add(Preprocessing.GetCenter(cnt));
            }

            Contours.Draw(calibImage, calibPtsContours);
            
            centers = Preprocessing.OrderPoints(centers);
            var centersMat = new MatOfPoint2f(centers.ToArray());
            
            H = Calib3d.findHomography(centersMat, Constants.realPointsMat);
        }

        /*
         * ___________________________________
         *          PRIVATE HELPERS
         * ___________________________________
         */
        protected void CalibrateImage(Mat srcImg)
        {
            // Log.AddImage(srcImg);
            Log.Add("Calibration Start");
            Imgproc.warpPerspective(srcImg, image, H, new Size(Constants.RefWarpedImgWidth, Constants.RefWarpedImgHeight));
            Log.Add("Perspective warped");
            
            // Log.AddImage(image);

            // Find center mark in the middle of the board.
            Log.Add("FindMark start.");
            markCoords = Calibration.FindMark(image);
            Log.Add("FindMark end.");
            
            // Calibrate areas
            Log.Add("Calibrate areas start.");
            CalibrateAreas();
            Log.Add("Calibrate areas end.");
        }

        protected void CalibrateAreas()
        {
            var dif = new List<double>{ Constants.REF_MARK_COORD[0] - markCoords.x, Constants.REF_MARK_COORD[1] - markCoords.y };
            
            Log.Add(Math.Abs(dif[0]) + "   " + Math.Abs(dif[1]));
            if (Math.Abs(dif[0]) > 1000 || Math.Abs(dif[1]) > 1000)
            {
                throw new InvalidProgramException("Hrací podložka není celá vidět!");
            }
            config.CalibrateAreas(dif, image);
        }

        /*
        * ___________________________________
        *           PUBLIC API
        * ___________________________________
        */
        public abstract Dictionary<AreaType, List<Tile>> GetObjectsInfo(Mat image);
    }
}
