using UnityEngine;

public static class ResolutionExt
{

    public static int ToInt(this Resolution resolution)
    {
        return resolution.width << 16 | resolution.height;
    }

    public static Resolution FromInt(int i)
    {
        return new Resolution {width = i >> 16, height = i & 0xFFFF};
    }

}
