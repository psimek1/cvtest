using System.Collections.Generic;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.UnityUtils;
using UnityEngine;
using UnityEngine.UI;

public class Log : MonoBehaviour
{

    private static int MaxLogLength = 10000;
    
    public static void Add(string message)
    {
        instance?.AddToLog(message);
    }
    
    public static void AddLine()
    {
        instance?.AddToLog("");
    }

    public static void AddImage(Mat mat)
    {
        instance?.AddImageToLog(mat);
    }

    public static void ClearImages()
    {
        instance?.ClearImageLog();
    }

    private static Log instance;
    
    [SerializeField]
    private Text text;

    [SerializeField] 
    private ScrollRect scrollRect;

    [SerializeField]
    private ImageLog imageLog;
    
    private Queue<string> textQueue;

    private Queue<Mat> matQueue;

    private bool clearImageLog;
    
    private string log;

    private short frameCounter;
    
    private void Awake()
    {
        textQueue = new Queue<string>();
        matQueue = new Queue<Mat>();
        clearImageLog = false;
        instance = this;
        log = "";
        frameCounter = -1;
    }

    private void AddToLog(string message)
    {
        textQueue.Enqueue(message);
    }
    
    private void AddImageToLog(Mat mat)
    {
        matQueue.Enqueue(mat);
    }

    private void ClearImageLog()
    {
        clearImageLog = true;
    }
    
    private void Update()
    {
        if (clearImageLog)
        {
            imageLog?.ClearImages();
            clearImageLog = false;
        }
        
        while (matQueue.Count > 0)
        {
            imageLog?.AddImage(matQueue.Dequeue());
        }
        
        while (textQueue.Count > 0)
        {
            log += textQueue.Dequeue() + "\n";
            if (log.Length > MaxLogLength)
            {
                log = log.Substring(log.Length - MaxLogLength, MaxLogLength);
            }
            text.text = log;
            frameCounter = 1;
        }

        if (frameCounter < 0) return;

        if (frameCounter == 0)
        {
            scrollRect.normalizedPosition = Vector2.zero;
        }

        frameCounter--;
    }
}
