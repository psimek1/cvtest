using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace ModuleWrappers
{
    public abstract class ModuleWrapper : MonoBehaviour, IOnWebCamStart, IOnWebCamStop, IOnModeSelected
    {

        private const float StartDelay = 5f;
        
        protected abstract RecognitionMode Mode { get; }

        private bool isModeSelected;
        
        private WebCamTexture webCamTexture;
        
        private bool isWebCamRunning;

        private Queue<WebCamTexture> webCamQueue;
        
        private bool isStopModuleRequest;

        private bool isModuleStopped;
        
        private bool isRunning;
        
        private Thread moduleThread;
        
        private bool webCamTexturePreprocessed;

        private Exception moduleException;

        private bool webcamReady = false;

        private void Awake()
        {
            webCamQueue = new Queue<WebCamTexture>();
        }

        public void OnWebCamStart(WebCamDevice webCamDevice, Resolution resolution, WebCamTexture aWebCamTexture)
        {
            StartCoroutine(OnWebCamStartCoroutine(aWebCamTexture));
        }

        private IEnumerator OnWebCamStartCoroutine(WebCamTexture aWebCamTexture)
        {
            yield return new WaitForSeconds(StartDelay);
            webCamQueue.Enqueue(aWebCamTexture);
            webcamReady = true;
        }

        public void OnWebCamStop()
        {
            webCamQueue.Enqueue(null);
        }
        
        public void OnModeSelected(RecognitionMode aMode)
        {
            isModeSelected = aMode == Mode;
        }

        private void OnApplicationQuit()
        {
            isStopModuleRequest = true;
        }

        /// <summary>
        /// Main thread loop
        /// </summary>
        private void Update()
        {
            if (webCamQueue.Count > 0)
            {
                webCamTexture = webCamQueue.Dequeue();
                isWebCamRunning = webCamTexture != null;
            }
            
            if (isRunning)
            {
                if (isModuleStopped)
                {
                    moduleThread.Abort();
                    moduleThread = null;
                    isRunning = false;
                    isModuleStopped = false;
                }
                else if (!isModeSelected || !isWebCamRunning || isStopModuleRequest)
                {
                    isStopModuleRequest = true;
                }
                else
                {
                    if (!webCamTexturePreprocessed && webcamReady)
                    {
                        OnPreprocessWebCamTexture(webCamTexture);
                        webCamTexturePreprocessed = true;
                    }
                }
            }
            else
            {
                if (isModeSelected && isWebCamRunning && !isStopModuleRequest && webcamReady)
                {
                    isRunning = true;
                    OnPreprocessWebCamTexture(webCamTexture);
                    OnInitModule();
                    moduleThread = new Thread(RunModule);
                    moduleThread.Start();
                    webCamTexturePreprocessed = false;
                }
            }

            if (moduleException != null)
            {
                Log.Add(moduleException.ToString());
                moduleException = null;
            }
        }

        private void RunModule()
        {
            OnStartModule();
            while (true)
            {
                Log.Add("You have 5 seconds to setup tiles.");
                Thread.Sleep(5000);
                if (isStopModuleRequest)
                {
                    isStopModuleRequest = false;
                    OnStopModule();
                    isModuleStopped = true;
                    break;
                }
                try
                {
                    if (webCamTexturePreprocessed)
                    {
                        Log.Add("About to call On Update Module");
                        OnUpdateModule();
                        webCamTexturePreprocessed = false;
                    }
                    Thread.Sleep(1000);
                }
                catch (Exception e)
                {
                    moduleException = e;
                }
            }
        }
        
        protected abstract void OnInitModule();
        
        protected abstract void OnStartModule();
        
        protected abstract void OnPreprocessWebCamTexture(WebCamTexture webCamTexture);

        protected abstract void OnUpdateModule();
        
        protected abstract void OnStopModule();

    }
}
