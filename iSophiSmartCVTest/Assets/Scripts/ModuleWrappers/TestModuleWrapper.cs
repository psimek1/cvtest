using System.Threading;
using UnityEngine;

namespace ModuleWrappers
{
    public class TestModuleWrapper : ModuleWrapper
    {
        protected override RecognitionMode Mode => RecognitionMode.Test;

        /// <summary>
        /// Called on module init.
        /// Called in main thread.
        /// </summary>
        protected override void OnInitModule()
        {

        }

        /// <summary>
        /// Called on module start.
        /// Called in module thread.
        /// </summary>
        protected override void OnStartModule()
        {
            Log.Add("TestModuleWrapper.OnStartModule");
        }

        /// <summary>
        /// Called when the new camera image is available.
        /// Called in main thread.
        /// </summary>
        /// <param name="webCamTexture">Camera texture</param>
        protected override void OnPreprocessWebCamTexture(WebCamTexture webCamTexture)
        {
            Log.Add("TestModuleWrapper.OnPreprocessWebCamTexture");
        }
        
        /// <summary>
        /// Called each time the new camera image is available.
        /// Called in module thread.
        /// </summary>
        protected override void OnUpdateModule()
        {
            for (int i = 0; i <= 100; i++)
            {
                Log.Add($"TestModuleWrapper.OnUpdateModule ({i} %)");
                Thread.Sleep(100);
            }
        }

        /// <summary>
        /// Called on module end.
        /// Called in module thread.
        /// </summary>
        protected override void OnStopModule()
        {
            Log.Add("TestModuleWrapper.OnStopModule");
        }
        
    }
}
