using OpenCVForUnity.CoreModule;
using OpenCVForUnity.UnityUtils;
using UnityEngine;
using System.Collections.Generic;
using System;
using CVModule;

namespace ModuleWrappers
{
    public class CVModuleWrapper : ModuleWrapper
    {
        
        protected override RecognitionMode Mode => RecognitionMode.CV;

        private int width, height;

        private Color32[] bufferColors;
        
        AlgoModule module;
        
        private Mat bufferMat;
        private Mat mat;
        private Mat mockCalibImg;
        private Mat mockTest;
        private bool i1Loaded = false;

        private bool isTablet = false;

        /// <summary>
        /// Called on module init.
        /// Called in main thread.
        /// </summary>
        protected override void OnInitModule()
        {
            Log.Add("INIT START");
            mockCalibImg = new Mat();
            mockTest = new Mat();
            
            ResourcesUtils.LoadImage(ResourcesUtils.ResourcesMode.BuiltInResources, "calibImg", texture =>
            {
                mockCalibImg = new Mat(texture.height, texture.width, CvType.CV_8UC3);
                Utils.texture2DToMat(texture, mockCalibImg);
                Log.Add("i1 loaded");
                try
                {
                    List<Area> areas = new List<Area>
                    {
                        new Area(AreaType.Grid4X4,
                            new List<CvTile>
                                {
                                    CvTile.SqFullRed,
                                    CvTile.SqFullGreen,
                                    CvTile.SqFullYellow,
                                    CvTile.SqDiagRed,
                                    CvTile.SqDiagYellow,
                                    CvTile.SqDiagGreen,
                                    CvTile.SqStripeYellow
                                    // CvTile.SqStripeGreen,
                                })
                    };
                    Config conf =
                        new Config(areas, new Dictionary<string, string> { });
            
                    module = new CVModule.AlgoModule(conf, mockCalibImg);
                }
                catch (Exception e)
                {
                    Log.Add(e.ToString());
                }
            });
            ResourcesUtils.LoadImage(ResourcesUtils.ResourcesMode.BuiltInResources, "test3", texture =>
            {
                mockTest = new Mat(texture.height, texture.width, CvType.CV_8UC3);
                Utils.texture2DToMat(texture, mockTest);
                i1Loaded = true;
            });
            
            // try
            // {
            //     List<Area> areas = new List<Area>
            //     {
            //         new Area(AreaType.Grid4X4,
            //             new List<CvTile>
            //                 {
            //                     CvTile.SqFullYellow,
            //                     CvTile.SqFullBlue,
            //                     CvTile.SqFullGreen,
            //                     CvTile.SqFullRed,
            //                     CvTile.SqStripeYellow
            //                 })
            //     };
            //     Config conf =
            //         new Config(areas, new Dictionary<string, string> { });
            //
            //     module = new AlgoModule(conf, mat.clone());
            //     Log.Add("module passed");
            // }
            // catch (Exception e)
            // {
            //     Log.Add(e.ToString());
            // }
            //
            // // Log.Add("Init successfull = " + !module.failedInit);
            // Log.Add("INIT END");
        }

        /// <summary>
        /// Called on module start.
        /// Called in module thread.
        /// </summary>
        protected override void OnStartModule()
        {
            Log.Add("CV module started.");
        }

        /// <summary>
        /// Called when the new camera image is available.
        /// Called in main thread.
        /// </summary>
        /// <param name="webCamTexture">Camera texture</param>
        protected override void OnPreprocessWebCamTexture(WebCamTexture webCamTexture)
        {
            if (bufferMat == null)
            {
                bufferMat = new Mat(webCamTexture.height, webCamTexture.width, CvType.CV_8UC3);
                bufferColors = webCamTexture.GetPixels32();
            }
            Utils.webCamTextureToMat(webCamTexture, bufferMat, bufferColors);

            // Tablet funcs
            mat = bufferMat.clone();
            if (isTablet)
            {
                Mat tmp = new Mat();
                Core.rotate(mat, tmp, Core.ROTATE_90_COUNTERCLOCKWISE);
                Core.flip(tmp, mat, 1);
                tmp.Dispose();
            }
        }
        
        /// <summary>
        /// Called when the new camera image is pre-processed and ready to be handled by the module 
        /// Called in module thread.
        /// </summary>
        protected override void OnUpdateModule()
        {
            // if (module.failedInit) { return; }
            // try
            // {
            //     var areaFoundTiles = module.GetObjectsInfo(mat.clone());
            //     foreach (var at in areaFoundTiles)
            //     {
            //         Log.Add(at.Key.GetString() + ":\n");
            //         foreach (var tile in at.Value)
            //         {
            //             Log.Add("\t" + tile.cvTile);
            //         }
            //     }
            // }
            // catch (Exception e)
            // {
            //     Log.Add(e.ToString());
            // }
            
            if (!mockCalibImg.empty() && !mockTest.empty())
            {
                if (i1Loaded)
                {
                    try
                    {
                        var areaFoundTiles = module.GetObjectsInfo(mockTest.clone());
                        foreach (var at in areaFoundTiles)
                        {
                            Log.Add(at.Key.GetString() + ":");
                            foreach (var tile in at.Value)
                            {
                                Log.Add("\t" + tile.cvTile + ", grid pos:" + tile.gridCoords.Item1 + ", " + tile.gridCoords.Item2);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Log.Add(e.ToString());
                    }
                }
            }
        }

        /// <summary>
        /// Called on module end.
        /// Called in module thread.
        /// </summary>
        protected override void OnStopModule()
        {
            Log.Add("CV module stopped.");

            mat = null;
            bufferColors = null;
        }
        
    }
}