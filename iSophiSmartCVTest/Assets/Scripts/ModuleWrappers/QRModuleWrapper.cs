using UnityEngine;
using ZXing;

namespace ModuleWrappers
{
    public class QRModuleWrapper : ModuleWrapper
    {
        protected override RecognitionMode Mode => RecognitionMode.QR;

        private BarcodeReader barcodeReader;

        private Color32[] camPixels;

        private int width, height;

        /// <summary>
        /// Called on module init.
        /// Called in main thread.
        /// </summary>
        protected override void OnInitModule()
        {

        }

        /// <summary>
        /// Called on module start.
        /// Called in module thread.
        /// </summary>
        protected override void OnStartModule()
        {
            Log.Add("QR module started.");
            
            barcodeReader = new BarcodeReader { AutoRotate = false };
        }

        /// <summary>
        /// Called when the new camera image is available.
        /// Called in main thread.
        /// </summary>
        /// <param name="webCamTexture">Camera texture</param>
        protected override void OnPreprocessWebCamTexture(WebCamTexture webCamTexture)
        {
            camPixels = webCamTexture.GetPixels32();
            width = webCamTexture.width;
            height = webCamTexture.height;
        }
        
        /// <summary>
        /// Called each time the new camera image is available.
        /// Called in module thread.
        /// </summary>
        protected override void OnUpdateModule()
        {
            var result = barcodeReader.Decode(camPixels, width, height);
            if (result != null)
            {
                Log.Add($"QR code detected: {result.Text}");
            }
        }

        /// <summary>
        /// Called on module end.
        /// Called in module thread.
        /// </summary>
        protected override void OnStopModule()
        {
            Log.Add("QR module stopped.");
        }
        
    }
}
