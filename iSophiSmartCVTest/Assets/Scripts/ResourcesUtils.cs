using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class ResourcesUtils: MonoBehaviour
{

    private static ResourcesUtils instance;
    
    public enum ResourcesMode
    {
        BuiltInResources, // for resources placed in "Resources" folder
        StreamingAssets // for resources placed in StreamingAssets folder
    }
    
    public static void LoadText(ResourcesMode resourcesMode, string resourceName, Action<string> onLoad)
    {
        switch (resourcesMode)
        {
            case ResourcesMode.BuiltInResources:
                var textAsset = Resources.Load<TextAsset>(resourceName);
                onLoad(textAsset == null ? null : textAsset.text);
                break;
            case ResourcesMode.StreamingAssets:
                instance.StartCoroutine(instance.LoadFromStreamingAssets(resourceName, handler =>
                    {
                        onLoad(handler?.text);
                    }));
                break;
        }
    }
    
    public static void LoadImage(ResourcesMode resourcesMode, string resourceName, Action<Texture2D> onLoad)
    {
        switch (resourcesMode)
        {
            case ResourcesMode.BuiltInResources:
                var textureAsset = Resources.Load<Texture2D>(resourceName);
                onLoad(textureAsset == null ? null : textureAsset);
                break;
            case ResourcesMode.StreamingAssets:
                instance.StartCoroutine(instance.LoadFromStreamingAssets(resourceName, handler =>
                {
                    if (handler == null)
                    {
                        onLoad(null);
                    }
                    else
                    {
                        var texture = new Texture2D(1, 1);
                        texture.LoadImage(handler.data);
                        onLoad(texture);    
                    }
                }));
                break;
        }
    }

    private void Awake()
    {
        instance = this;
    }

    private IEnumerator LoadFromStreamingAssets(string resourceName, Action<DownloadHandler> onLoad)
    {
        var uri = Path.Combine(Application.streamingAssetsPath, resourceName);

        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            yield return webRequest.SendWebRequest();
            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                case UnityWebRequest.Result.ProtocolError:
                    onLoad(null);
                    break;
                case UnityWebRequest.Result.Success:
                    onLoad(webRequest.downloadHandler);
                    break;
            }
        }
    }

}
