using System;
using System.Linq;
using UnityEngine;

public class Main: MonoBehaviour, IOnWebCamDevicesAdded, IOnWebCamDeviceClick, IOnResolutionSelected, IOnModeClick, IOnImageLogClick
{

    private const string Version = "0.12";

    public const string Resolution = "Resolution";

    [SerializeField]
    private GameObject imageLog;
    
    private RecognitionMode mode;
    
    private WebCamTexture webCamTexture;

    private WebCamDevice currentWebCamDevice;

    private Resolution currentResolution;

    private bool webCamRunning;

    private void Start()
    {
        Log.Add($"CVTest {Version}");
    }

    public void OnWebCamDevicesAdded()
    {
        Log.Add($"All devices added. To start capture, please select one of the devices from the list on top and then select resolution.\n");

        SetMode(RecognitionMode.CV);
    }
    
    public void OnWebCamDeviceClick(WebCamDevice webCamDevice)
    {
        if (webCamRunning)
        {
            StopWebCam();
        }

        currentWebCamDevice = webCamDevice.Equals(currentWebCamDevice) ? new WebCamDevice() : webCamDevice;
        
        ExecuteInChildren<IOnWebCamDeviceSelected>(handler => handler.OnWebCamDeviceSelected(currentWebCamDevice));
    }

    public void OnResolutionSelected(Resolution resolution)
    {
        if (webCamRunning)
        {
            StopWebCam();
        }

        SetResolution(resolution);
        
        webCamTexture = new WebCamTexture(currentWebCamDevice.name)
            {requestedWidth = resolution.width, requestedHeight = resolution.height};
    
        webCamTexture.Play();

        ExecuteInChildren<IOnWebCamStart>(handler => handler.OnWebCamStart(currentWebCamDevice, resolution, webCamTexture));
        
        Log.Add($"{currentWebCamDevice.name} camera started at {currentResolution.ToString()}");

        webCamRunning = true;
    }

    private void SetResolution(Resolution resolution)
    {
        currentResolution = resolution;
        if (resolution.width == 0) return;
        PlayerPrefs.SetInt(Resolution + currentWebCamDevice.name, resolution.ToInt());
        PlayerPrefs.Save();
    }

    private void StopWebCam()
    {
        webCamTexture?.Stop();
        ExecuteInChildren<IOnWebCamStop>(handler => handler.OnWebCamStop());
        Log.Add($"{currentWebCamDevice.name} camera stopped.");
        webCamRunning = false;
    }
    
    public void OnModeClick(RecognitionMode aMode)
    {
        if (mode == aMode) return;
        SetMode(aMode);
    }

    private void SetMode(RecognitionMode aMode)
    {
        mode = aMode;
        
        ExecuteInChildren<IOnModeSelected>(handler => handler.OnModeSelected(mode));
    }
    
    private void ExecuteInChildren<T>(Action<T> action) where T : class
    {
        foreach (var monoBehaviour in GetComponentsInChildren<MonoBehaviour>().Where(b => b is T && b != this))
        {
            action(monoBehaviour as T);
        }
    }

    public void OnImageModeClick()
    {
        imageLog.SetActive(!imageLog.activeSelf);
    }
}
